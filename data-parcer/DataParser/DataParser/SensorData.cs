﻿namespace DataParser
{
    public class SensorData
    {
        public string EUI { get; set; }
        public bool offline { get; set; }
        public string data { get; set; }
    }

    public class SensorValues
    {
        public int firstSensorValue { get; set; }
        public int secondSensorValue { get; set; }
        public float temperature { get; set; }
        public int battery { get; set; }
        public DateTime timeStamp { get; set; }
    }

    public class SensorsTimeStampedValues
    {
        public SensorValues SensorValues40MinsAgo { get; set; }
        public SensorValues SensorValues20MinsAgo { get; set; }
        public SensorValues SensorValuesCurrentTime { get; set; }
    }
}

﻿using DataParser.Persistence.Entities.Company;
using DataParser.Persistence.Repositories;
using DeviceEmulator;
using MassTransit;

namespace DataParser
{
    public class DeviceDataConsumer : IConsumer<DeviceData>
    {
        private readonly IIoDitRepository _repository;

        public DeviceDataConsumer(IIoDitRepository repository)
        {
            _repository = repository;
        }

        public async Task Consume(ConsumeContext<DeviceData> context)
        {
            var deviceData = context.Message;
            var device = await _repository.GetDeviceByEui(deviceEui: deviceData.DeviceName);
            if (device != null)
            {
                var companyDeviceData = new CompanyDeviceData()
                {
                        Device = device,
                        DeviceId = device.Id,
                        BatteryLevel = deviceData.Message.BatteryLevel,
                        Sensor1 = deviceData.Message.Sensor1,
                        Sensor2 = deviceData.Message.Sensor2,
                        Temperature = deviceData.Message.Temperature,
                        TimeStamp = deviceData.Message.TimeStamp,
                };
                var createdCompanyDeviceData = await _repository.CreateAsync(companyDeviceData);
                var message = $"Data for Device: {deviceData.DeviceName}\n" +
                              $"Time: {createdCompanyDeviceData.TimeStamp}\n" +
                              $"Sensor1: {createdCompanyDeviceData.Sensor1}\n" +
                              $"Sensor2: {createdCompanyDeviceData.Sensor2}\n" +
                              $"Temperature: {createdCompanyDeviceData.Temperature}\n" +
                              $"BatteryLevel: {createdCompanyDeviceData.BatteryLevel}\n";
                await Console.Out.WriteLineAsync(message);
            }

            
        }
    }
}
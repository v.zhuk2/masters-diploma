﻿namespace DataParser.Persistence.Entities.Base;

public class EntityBase
{
    public long Id { get; set; }
}
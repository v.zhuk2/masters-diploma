﻿using DataParser.Persistence.Entities.Base;

namespace DataParser.Persistence.Entities.Company;

public class CompanyUser : EntityBase, IEntity
{
    public long UserId { get; set; }
    public User User { get; set; }
    public long CompanyId { get; set; }
    public Company Company { get; set; }
    public bool IsDefault { get; set; }
    public ICollection<CompanyFarm> CompanyFarms { get; set; } = new List<CompanyFarm>();

}
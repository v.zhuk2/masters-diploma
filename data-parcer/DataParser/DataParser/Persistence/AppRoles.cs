﻿namespace DataParser.Persistence;

public enum AppRoles
{
    AppUser = 0,
    AppAdmin = 1
}
﻿using DataParser.Persistence;
using DataParser.Persistence.Repositories;
using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DataParser;

public static class Program
{
    public static async Task Main(string[] args)
    {
        var host = Host.CreateDefaultBuilder(args)
            .ConfigureServices(services =>
            {
                services.AddMassTransit(x =>
                {
                    x.AddConsumer<DeviceDataConsumer>();
                    x.UsingRabbitMq((context, config) =>
                    {
                        config.Host("localhost", "/", h =>
                        {
                            h.Username("guest");
                            h.Password("guest");
                        });
                        config.ReceiveEndpoint("device-data", e =>
                        {
                            e.ConfigureConsumer<DeviceDataConsumer>(context);
                        });
                    });
                });
                services.AddDbContext<IoDitDbContext>();
                services.AddScoped<IIoDitRepository, IoDitRepository>();
                services.AddScoped<DeviceDataConsumer>();
            })
            .Build();
        await host.RunAsync();
    }
}
﻿namespace DeviceEmulator;

public class DeviceData
{
    public string DeviceName { get; set; }
    public SensorData Message { get; set; }
}

public class SensorData
{
    public DateTime TimeStamp { get; set; }
    public int Sensor1 { get; set; }
    public int Sensor2 { get; set; }
    public int BatteryLevel { get; set; }
    public float Temperature { get; set; }
}
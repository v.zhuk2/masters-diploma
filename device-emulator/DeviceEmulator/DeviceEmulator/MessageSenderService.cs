﻿using MassTransit;
using Microsoft.Extensions.Hosting;

namespace DeviceEmulator
{
    public class MessageSenderService : IHostedService
    {
        private readonly IBusControl _busControl;

        public MessageSenderService(IBusControl busControl)
        {
            _busControl = busControl;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Message sender started.");
            var devices = DevicesList();

            while (true)
            {
                for (int i = 0; i < devices.Count; i++)
                {
                    var multiplier = 1 + (double)i / 10; 
                    var message = GetSensorValue(multiplier);
                    var dataToPublish = new DeviceData
                    {
                        DeviceName = devices[i],
                        Message = message
                    };
                    await _busControl.Publish(dataToPublish, cancellationToken);
                    Console.WriteLine($"Sent message for {devices[i]}.");
                }
                await Task.Delay(1000 * 60 * 15, cancellationToken);
            }
        }
        
        public Task StopAsync(CancellationToken cancellationToken)
        {
            Console.WriteLine("Message sender stopped.");
            return Task.CompletedTask;
        }

        private List<string> DevicesList()
        {
            var devices = new List<string>()
            {
                "3A857E405F8CB69E", 
                "53CAB97884ABF9B9", 
                "C93E107A1A620B50",
                "1624618204F4E2D5",
                "3E9C4B7B9C2D9417",
                "67EDD6F42DC81E71",
                "A78E37F80F288433",
                "D7B62A838D9F1E91",
                "BA0BD223C95217EB",
                "4627850CAD73A5A5",
            };
            return devices;
        }


        private SensorData GetSensorValue(double multiplier)
        {
            var currentDate = DateTime.Now; // Start from today's date
            var currentHour = currentDate.Hour;
            var sensor1 = Convert.ToInt32(currentHour * multiplier) + 10;
            var sensor2 = Convert.ToInt32(currentHour * multiplier) + 8;
            return new SensorData
            {
                TimeStamp = currentDate,
                Sensor1 = sensor1,
                Sensor2 = sensor2,
                BatteryLevel = 100 - currentHour * 4,
                Temperature = currentHour <= 12 ? currentHour + 10 : 24 - (24 - currentHour),
            };
        }
    }
}



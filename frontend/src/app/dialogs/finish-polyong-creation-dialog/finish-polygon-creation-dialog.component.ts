import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {FormControl, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {CompanyFarm} from "../../utilities/types/CompanyTypes";
import {Store} from "@ngrx/store";
import {getFarms} from "../../store/farm/farm.selectors";

@Component({
  selector: 'app-finish-polygon-creation.dialog',
  templateUrl: './finish-polygon-creation-dialog.component.html',
  styleUrls: ['./finish-polygon-creation-dialog.component.scss'],
})
export class FinishPolygonCreationDialogComponent implements OnInit {
  nameControl = new FormControl('', [Validators.required]);
  farmControl = new FormControl(null, [Validators.required]);
  $companyFarms: Observable<CompanyFarm[]>
  constructor(private readonly dialogRef: MatDialogRef<FinishPolygonCreationDialogComponent>,
              private readonly store: Store) { }

  ngOnInit() {
    this.$companyFarms = this.store.select(getFarms)
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    this.dialogRef.close({name: this.nameControl.value, farmId: this.farmControl.value});
  }

  isValid(): boolean {
    return this.farmControl.valid && this.nameControl.valid
  }
}

import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-request-company.dialog',
  templateUrl: './request-company-dialog.component.html',
  styleUrls: ['./request-company-dialog.component.scss'],
})
export class RequestCompanyDialogComponent implements OnInit {
  public nameControl = new FormControl('', [Validators.required]);
  public maxDevicesControl = new FormControl(10, [Validators.required]);

  constructor(private readonly dialogRef: MatDialogRef<RequestCompanyDialogComponent>) {
  }

  ngOnInit() {
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    this.dialogRef.close({
      name: this.nameControl.value,
      maxDevices: this.maxDevicesControl.value
    });
  }

  isValid(): boolean {
    return this.nameControl.valid && this.maxDevicesControl.valid
  }
}

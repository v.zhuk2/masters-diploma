import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {CompanyFarm, CreateDevice} from "../../../utilities/types/CompanyTypes";
import {getFarms} from "../../../store/farm/farm.selectors";
import {Store} from "@ngrx/store";
import {FormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {getCurrentCompanyUser} from "../../../store/user-company-user/user-company-user.selectors";

@Component({
  selector: 'app-create-device-dialog',
  templateUrl: './create-device-dialog.component.html',
  styleUrls: ['./create-device-dialog.component.scss'],
})
export class CreateDeviceDialogComponent  implements OnInit, OnDestroy  {
  public nameControl = new FormControl('', [Validators.required]);
  public deviceEUIControl = new FormControl('', [Validators.required, Validators.maxLength(16), Validators.minLength(16)]);
  public farmControl = new FormControl(null, [Validators.required]);
  public DefaultHumidity1Min = new FormControl(1, [Validators.required]);
  public DefaultHumidity1Max = new FormControl(100, [Validators.required]);
  public DefaultHumidity2Min = new FormControl(1, [Validators.required]);
  public DefaultHumidity2Max = new FormControl(100, [Validators.required]);
  public DefaultBatteryLevelMin = new FormControl(1, [Validators.required]);
  public DefaultBatteryLevelMax = new FormControl(100, [Validators.required]);
  public DefaultTemperatureMin = new FormControl(-20, [Validators.required]);
  public DefaultTemperatureMax = new FormControl(30, [Validators.required]);
  public min = 1;
  public max = 100;
  $companyFarms: Observable<CompanyFarm[]>
  public companyUserId: number
  public companyId: number
  private companyUserSubscription?: Subscription;


  constructor(private readonly dialogRef: MatDialogRef<CreateDeviceDialogComponent>,
              private readonly store: Store) { }

  //todo add threshold presets
  ngOnInit() {
    this.$companyFarms = this.store.select(getFarms)
    this.companyUserSubscription = this.store.select(getCurrentCompanyUser).subscribe(x => {
      this.companyUserId = x.id;
      this.companyId = x.companyId;
    });
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    let dialogResult: CreateDevice = {
      deviceEUI: this.deviceEUIControl.value,
      deviceName: this.nameControl.value,
      farmId: this.farmControl.value,
      companyId: this.companyId,
      companyUserId: this.companyUserId,
      defaultHumidity1Min: this.DefaultHumidity1Min.value,
      defaultHumidity1Max: this.DefaultHumidity1Max.value,
      defaultHumidity2Min: this.DefaultHumidity2Min.value,
      defaultHumidity2Max: this.DefaultHumidity2Max.value,
      defaultBatteryLevelMin: this.DefaultBatteryLevelMin.value,
      defaultBatteryLevelMax: this.DefaultBatteryLevelMax.value,
      defaultTemperatureMin: this.DefaultTemperatureMin.value,
      defaultTemperatureMax: this.DefaultTemperatureMax.value,
    }
    this.dialogRef.close(dialogResult);
  }

  isValid(): boolean {
    return this.nameControl.valid &&
      this.deviceEUIControl.valid &&
      this.farmControl.valid &&
      this.DefaultHumidity1Min.valid &&
      this.DefaultHumidity1Max.valid &&
      this.DefaultHumidity2Min.valid &&
      this.DefaultHumidity2Max.valid &&
      this.DefaultBatteryLevelMin.valid &&
      this.DefaultBatteryLevelMax.valid &&
      this.DefaultTemperatureMin.valid &&
      this.DefaultTemperatureMax.valid;
  }

  ngOnDestroy() {
    if (this.companyUserSubscription) {
      this.companyUserSubscription.unsubscribe();
    }
  }

}

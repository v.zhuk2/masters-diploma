import {Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {getDevices} from "../../store/device/device.selectors";
import {Observable} from "rxjs";
import {
  CreateDevice,
  DeviceDataSource
} from "../../utilities/types/CompanyTypes";
import {getFarms} from "../../store/farm/farm.selectors";
import {CreateDeviceDialogComponent} from "./create-device-dialog/create-device-dialog.component";
import {combineLatestWith, distinctUntilChanged, filter, map} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {createDevice} from "../../store/device/device.actions";
import {getFields} from "../../store/field/field.selectors";

@Component({
  selector: 'app-company-devices',
  templateUrl: './company-devices.component.html',
  styleUrls: ['./company-devices.component.scss'],
})
export class CompanyDevicesComponent  implements OnInit, OnDestroy {
  displayedColumns: string[] = ['name', 'farmName', 'fieldName'];
  $dataSource: Observable<DeviceDataSource[]>;

  constructor(private readonly store: Store, private readonly dialog: MatDialog) { }

  ngOnInit() {
    this.$dataSource = this.store.select(getDevices).pipe(
      combineLatestWith(
        this.store.select(getFarms),
        this.store.select(getFields)
        ),
      filter(([devices, farms, fields]) => {
        return devices != null && farms != null && fields != null
      }),
      map(([devices, farms, fields]) => {
        return devices.map(device => {
          let deviceField = fields.find(x => x.id == device.fieldId)
          let deviceFarm = farms.find(x => x.id == device.farmId)
          let dataSource: DeviceDataSource = {
            id: device.id,
            name: device.name,
            fieldId: device.fieldId,
            farmId: device.farmId,
            companyId: device.companyId,
            fieldName: deviceField?.name,
            farmName: deviceFarm?.name
          }
          return dataSource
        })
      }),
      distinctUntilChanged()
    );
  }
  ngOnDestroy(): void {
  }

  createDevice() {
    this.dialog.open(CreateDeviceDialogComponent, {
      width: '95%'
    }).afterClosed() .pipe(map(data => data as CreateDevice))
      .subscribe({
        next: data => {
          if (data != null) {
            this.store.dispatch(createDevice({device: data}))
          }
        },
        error: x => {
          console.log(x);
        }
      });
  }
}

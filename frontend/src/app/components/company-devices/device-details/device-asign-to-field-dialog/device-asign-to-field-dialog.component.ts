import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {FormControl, Validators} from "@angular/forms";
import {getFields} from "../../../../store/field/field.selectors";
import {Observable, Subscription} from "rxjs";
import {AssignToField, CompanyDevice, CompanyField} from "../../../../utilities/types/CompanyTypes";
import {combineLatestWith, filter, map} from "rxjs/operators";
import {getDevices} from "../../../../store/device/device.selectors";
import {getCurrentCompanyUser} from "../../../../store/user-company-user/user-company-user.selectors";

interface DialogData {
  deviceEui: string;
}

@Component({
  selector: 'app-device-asign-to-field-dialog',
  templateUrl: './device-asign-to-field-dialog.component.html',
  styleUrls: ['./device-asign-to-field-dialog.component.scss'],
})
export class DeviceAsignToFieldDialogComponent implements OnInit, OnDestroy {
  public fieldSelectControl = new FormControl('', [Validators.required]);
  public companyUserId: number
  public companyId: number
  public deviceEui: string
  $fields: Observable<CompanyField[]>;
  $devices: Observable<CompanyDevice[]>;
  private companyUserSubscription?: Subscription;

  constructor(private readonly dialogRef: MatDialogRef<DeviceAsignToFieldDialogComponent>,
              private readonly store: Store,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.deviceEui = data.deviceEui;
  }

  ngOnInit() {
    this.companyUserSubscription = this.store.select(getCurrentCompanyUser).subscribe(x => {
      this.companyUserId = x.id;
      this.companyId = x.companyId;
    });
    this.$fields = this.store.select(getFields);
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    let dialogResult: AssignToField = {
      companyId: this.companyId,
      companyUserId: this.companyUserId,
      deviceEui: this.deviceEui,
      fieldId: +this.fieldSelectControl.value
    }
    this.dialogRef.close(dialogResult);
  }

  isValid(): boolean {
    return this.fieldSelectControl.valid
  }

  ngOnDestroy() {
    if (this.companyUserSubscription) {
      this.companyUserSubscription.unsubscribe();
    }
  }

}

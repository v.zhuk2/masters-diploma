import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {getDeviceById} from "../../../store/device/device.selectors";
import {combineLatest, Observable, of, Subscription, switchMap} from "rxjs";
import {AssignToField, CompanyDeviceData, DeviceDetailedInfo} from "../../../utilities/types/CompanyTypes";
import {getFarmById} from "../../../store/farm/farm.selectors";
import {getFieldById} from "../../../store/field/field.selectors";
import {filter, map} from "rxjs/operators";
import {MatDialog} from "@angular/material/dialog";
import {DeviceAsignToFieldDialogComponent} from "./device-asign-to-field-dialog/device-asign-to-field-dialog.component";
import {assignDeviceToField} from "../../../store/device/device.actions";
import {FormControl} from "@angular/forms";
import {getCurrentCompanyUser} from "../../../store/user-company-user/user-company-user.selectors";
import {loadMoreDeviceData} from "../../../store/device-data/device-data.actions";
import {getDeviceDataByDeviceId} from "../../../store/device-data/device-data.selectors";

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.scss'],
})
export class DeviceDetailsComponent implements OnInit, OnDestroy {
  start = new FormControl()
  end = new FormControl()
  id: number;
  public companyUserId: number
  public companyId: number
  private companyUserSubscription?: Subscription;
  public $device: Observable<DeviceDetailedInfo>;
  public $deviceData: Observable<CompanyDeviceData[]>

  constructor(private route: ActivatedRoute, private readonly store: Store, private readonly dialog: MatDialog) {
  }

//todo also show some of last data
  ngOnInit() {
    this.companyUserSubscription = this.store.select(getCurrentCompanyUser).pipe(filter(x => !!x)).subscribe(x => {
      this.companyUserId = x.id;
      this.companyId = x.companyId;
    });
    this.id = +this.route.snapshot.paramMap.get('id');
    this.$deviceData = this.store.select(getDeviceDataByDeviceId(this.id))
      .pipe(filter(x => !!x),
        map(x => x.sort((a, b) => new Date(a.timeStamp).getTime() - new Date(b.timeStamp).getTime())))

    this.$device = this.store.select(getDeviceById(this.id)).pipe(filter(device => !!device), switchMap(device => {
      return combineLatest([of(device), this.store.select(getFarmById(device.farmId)), this.store.select(getFieldById(device.fieldId))]);
    }), map(([device, farm, field]) => {
      let res: DeviceDetailedInfo = {
        id: device.id,
        name: device.name,
        companyId: device.companyId,
        fieldId: device.fieldId,
        fieldName: field?.name,
        farmId: device.farmId,
        farmName: farm?.name,
        devEUI: device.devEUI,
        joinEUI: device.joinEUI,
        appKey: device.appKey,
        defaultHumidity1Min: device.defaultHumidity1Min,
        defaultHumidity1Max: device.defaultHumidity1Max,
        defaultHumidity2Min: device.defaultHumidity2Min,
        defaultHumidity2Max: device.defaultHumidity2Max,
        defaultBatteryLevelMin: device.defaultBatteryLevelMin,
        defaultBatteryLevelMax: device.defaultBatteryLevelMax,
        defaultTemperatureMin: device.defaultTemperatureMin,
        defaultTemperatureMax: device.defaultTemperatureMax,
        deviceData: device.deviceData
      }
      return res;
    }));
  }

  assignDeviceToField(eui: string) {
    this.dialog.open(DeviceAsignToFieldDialogComponent, {data: {deviceEui: eui}})
      .afterClosed()
      .pipe(map(data => data as AssignToField))
      .subscribe({
        next: data => {
          if (data != null) {
            console.log(data)
            this.store.dispatch(assignDeviceToField({device: data}))
          }
        }, error: x => {
          console.log(x);
        }
      });
  }

  LoadMoreDeviceData(devEUI: string) {
    this.store.dispatch(loadMoreDeviceData({
      device: {
        start: this.start.value, end: this.end.value, devEUI: devEUI, companyUserId: this.companyUserId
      }
    }))
  }

  ngOnDestroy() {
    if (this.companyUserSubscription) {
      this.companyUserSubscription.unsubscribe();
    }
  }
}

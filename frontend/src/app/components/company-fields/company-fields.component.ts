import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {CompanyField} from "../../utilities/types/CompanyTypes";
import {filter} from "rxjs/operators";
import {getFields} from "../../store/field/field.selectors";

@Component({
  selector: 'app-company-fields',
  templateUrl: './company-fields.component.html',
  styleUrls: ['./company-fields.component.scss'],
})
export class CompanyFieldsComponent  implements OnInit {
  $fields: Observable<CompanyField[]>;
  constructor(private readonly store: Store) { }

  ngOnInit() {
    this.$fields = this.store.select(getFields).pipe(filter(x => x != null))
  }
}

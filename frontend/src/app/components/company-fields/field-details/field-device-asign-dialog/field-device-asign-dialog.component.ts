import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {Observable, Subscription} from "rxjs";
import {AssignToField, CompanyDevice, CompanyField} from "../../../../utilities/types/CompanyTypes";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {getCurrentCompanyUser} from "../../../../store/user-company-user/user-company-user.selectors";
import {getFields} from "../../../../store/field/field.selectors";
import {
} from "../../../company-devices/device-details/device-asign-to-field-dialog/device-asign-to-field-dialog.component";
import {getDevices} from "../../../../store/device/device.selectors";
import {filter, map} from "rxjs/operators";

interface DialogData {
  fieldId: number
}

@Component({
  selector: 'app-field-device-asign-dialog',
  templateUrl: './field-device-asign-dialog.component.html',
  styleUrls: ['./field-device-asign-dialog.component.scss'],
})
export class FieldDeviceAsignDialogComponent  implements OnInit {
  public deviceSelectControl = new FormControl(null, [Validators.required]);
  public companyUserId: number
  public companyId: number
  public fieldId: number
  $fields: Observable<CompanyField[]>;
  $devices: Observable<CompanyDevice[]>;
  private companyUserSubscription?: Subscription;

  constructor(private readonly dialogRef: MatDialogRef<FieldDeviceAsignDialogComponent>,
              private readonly store: Store,
              @Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.fieldId = data.fieldId;
  }

  ngOnInit() {
    this.companyUserSubscription = this.store.select(getCurrentCompanyUser).subscribe(x => {
      this.companyUserId = x.id;
      this.companyId = x.companyId;
    });
    this.$devices = this.store.select(getDevices).pipe(filter(x => !!x), map(x => {
      return x.filter(z => z.fieldId == null)
    }));
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    let dialogResult: AssignToField = {
      companyId: this.companyId,
      companyUserId: this.companyUserId,
      deviceEui: this.deviceSelectControl.value,
      fieldId: this.fieldId
    }
    this.dialogRef.close(dialogResult);
  }

  isValid(): boolean {
    return this.deviceSelectControl.valid
  }

  ngOnDestroy() {
    if (this.companyUserSubscription) {
      this.companyUserSubscription.unsubscribe();
    }
  }

}

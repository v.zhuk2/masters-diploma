import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {AssignToField, CompanyDevice, CompanyField} from "../../../utilities/types/CompanyTypes";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {getDeviceByFieldId} from "../../../store/device/device.selectors";
import {getFieldById} from "../../../store/field/field.selectors";
import {map} from "rxjs/operators";
import {FieldDeviceAsignDialogComponent} from "./field-device-asign-dialog/field-device-asign-dialog.component";
import {assignDeviceToField} from "../../../store/device/device.actions";

@Component({
  selector: 'app-field-details',
  templateUrl: './field-details.component.html',
  styleUrls: ['./field-details.component.scss'],
})
export class FieldDetailsComponent  implements OnInit {
  id: number;
  $devices: Observable<CompanyDevice[]>
  $field: Observable<CompanyField>
  constructor(private route: ActivatedRoute, private readonly store: Store, private readonly dialog: MatDialog) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.$field = this.store.select(getFieldById(this.id))
    this.$devices = this.store.select(getDeviceByFieldId(this.id))
  }

  fieldDeviceAssign(fieldId: number) {
    this.dialog.open(FieldDeviceAsignDialogComponent, {data: {fieldId: fieldId}})
      .afterClosed()
      .pipe(map(data => data as AssignToField))
      .subscribe({
        next: data => {
          if (data != null) {
            this.store.dispatch(assignDeviceToField({device: data}))
          }
        }, error: x => {
          console.log(x);
        }
      });
  }
}

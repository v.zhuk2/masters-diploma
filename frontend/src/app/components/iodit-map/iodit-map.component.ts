import {AfterViewInit, Component, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {GoogleMap, MapInfoWindow, MapPolygon} from "@angular/google-maps";
import {MatDialog} from "@angular/material/dialog";
import {
  FinishPolygonCreationDialogComponent
} from "../../dialogs/finish-polyong-creation-dialog/finish-polygon-creation-dialog.component";
import * as turf from '@turf/turf';
import {combineLatestWith, distinctUntilChanged, filter, map} from 'rxjs/operators';
import {Store} from "@ngrx/store";
import {createField, updateGeofence} from "../../store/field/field.actions";
import {Observable, Subscription, tap} from "rxjs";
import {CompanyDevice, CompanyFarm, CompanyField, CompanyUser} from "../../utilities/types/CompanyTypes";
import {getFields, isLoadingFields} from '../../store/field/field.selectors';
import {getDevices, isLoadingDevices} from "../../store/device/device.selectors";
import {MapCompanyField} from "../../utilities/types/MapTypes";
import {getFarms, isLoadingFarms} from "../../store/farm/farm.selectors";
import {getCurrentCompanyUser} from "../../store/user-company-user/user-company-user.selectors";
import {FormControl} from "@angular/forms";


@Component({
  selector: 'app-iodit-map',
  templateUrl: './iodit-map.component.html',
  styleUrls: ['./iodit-map.component.scss'],
})
export class IoditMapComponent implements OnInit, AfterViewInit, OnDestroy {
  fieldInfoWindows: { field: MapCompanyField, infoWindow: MapInfoWindow }[] = [];
  fieldPolygons: { field: MapCompanyField, polygon: MapPolygon }[] = []

  @ViewChildren(MapPolygon) set mapPolygons(mapPolygons: QueryList<MapPolygon>) {
    let mapPolygonsArray = mapPolygons.toArray();
    if (this.fields != null && this.fields?.length) {
      mapPolygonsArray.forEach(googlePolygon => {
        googlePolygon.polygon.addListener('click', (mev) => this.deleteNode(mev, googlePolygon.polygon))
      })
      this.fieldPolygons = this.fields.map((field, index) => {
        return {field: field, polygon: mapPolygonsArray[index]};
      });
    }
  }
  @ViewChildren(MapInfoWindow) set infoWindows(infoWindows: QueryList<MapInfoWindow>) {
    let infoWindowsArray = infoWindows.toArray();
    if (this.fields != null && this.fields?.length) {
      this.fieldInfoWindows = this.fields.map((field, index) => {
        return {field: field, infoWindow: infoWindowsArray[index]};
      });
    }
  }

  @ViewChild(GoogleMap) mapRef: GoogleMap;

  public map: google.maps.Map;
  public options: google.maps.MapOptions;
  private drawingManager: google.maps.drawing.DrawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: null,
    drawingControl: true,
    drawingControlOptions: {
      position: google.maps.ControlPosition.TOP_CENTER,
      drawingModes: [google.maps.drawing.OverlayType.POLYGON]
    },
    polygonOptions: {
      draggable: true,
      editable: true
    },
  });
  //currentPositionMarker: google.maps.LatLngLiteral;

  private fields: MapCompanyField[]
  public farmFilterControl = new FormControl(null);
  public selectedFarm: MapCompanyField[] = [];
  public selectedField: MapCompanyField = null;

  private subscriptions: Subscription[] = []

  private $isLoading: Observable<boolean>
  public $companyUser: Observable<CompanyUser>
  public $farms: Observable<CompanyFarm[]>
  private $fields: Observable<CompanyField[]>
  private $devices: Observable<CompanyDevice[]>
  private $mapFields: Observable<MapCompanyField[]>
  public $filteredMapFields: Observable<MapCompanyField[]>

  constructor(private readonly dialog: MatDialog,
              private readonly store: Store) {}

  ngOnInit() {
    this.options = this.initMapOptions();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(x => x.unsubscribe());
  }

  async ngAfterViewInit() {
    this.initDrawingManager();
    this.$fields = this.store.select(getFields);
    this.$farms = this.store.select(getFarms);
    this.$devices = this.store.select(getDevices);
    this.$companyUser = this.store.select(getCurrentCompanyUser);

    this.$isLoading = this.store.select(isLoadingFarms).pipe(
      combineLatestWith(
        this.store.select(isLoadingFields),
        this.store.select(isLoadingDevices)),
      map(([isLoadingFarms, isLoadingDevices, isLoadingFields]) => {
        return isLoadingFarms || isLoadingDevices || isLoadingFields
      }),
      distinctUntilChanged()
    );

    this.$mapFields = this.$fields.pipe(
      combineLatestWith(this.$devices, this.$isLoading),
      filter(([fields, devices, isLoading]) => !isLoading),
      map(([fields, devices, isLoading]) => {
        this.fields = [];
        return fields.map(field => {
          const transformedField = this.transformField(field, devices);
          if (!this.fields.some(f => f.id === transformedField.id)) {
            this.fields.push(transformedField);
          }
          return transformedField;
        })
      })
    );
    this.$filteredMapFields = this.$mapFields;

    this.farmFilterControl.valueChanges.subscribe(x => {
      if (!x) {
        this.$filteredMapFields = this.$mapFields
        return
      }
      this.applyFarmFilter();
    })
  }

  applyFarmFilter() {
    let selectedFarmId = this.farmFilterControl.value;

    this.$filteredMapFields = this.$mapFields.pipe(
      map(fields => fields.filter(field => field.companyFarmId === selectedFarmId)),
      tap(fields => this.selectedFarm = fields)
    );

    this.subscriptions.push(this.$filteredMapFields.subscribe());
  }

  private initMapOptions(): google.maps.MapOptions {
    return {
      mapTypeId: 'hybrid',
      streetViewControl: false,
      fullscreenControl: false,
      rotateControl: false,
      mapTypeControl: false,
      center: {lat: 49.243152, lng: 28.436217},
      restriction: {strictBounds: true, latLngBounds: {
          north: 85,
          south: -85,
          east: 180,
          west: -180,
        }},
      styles: [
        {
          featureType: 'poi',
          elementType: 'labels',
          stylers: [{visibility: 'off'}]
        }
      ]
    };
  }

  private initDrawingManager() {
    this.drawingManager.setMap(this.mapRef.googleMap);
    google.maps.event.addListener(this.drawingManager, 'polygoncomplete', (polygon: google.maps.Polygon) => this.finishPolygonCreation(polygon));
  }

  deleteNode(event: google.maps.PolyMouseEvent, polygon: google.maps.Polygon) {
    let paths = polygon.getPath().getArray()
    if (event.vertex != null && paths.length > 3) {
      polygon.getPath().removeAt(event.vertex);
    }
  }

  onMapClick($event: google.maps.MapMouseEvent | google.maps.IconMouseEvent) {
    this.closeInfoWindows();
  }

  finishPolygonUpdate(companyFarmId: number, fieldId: number, companyUserId: number) {
    this.closeInfoWindows();
    let googlePolygon = this.fieldPolygons.find(x => x.field.id == fieldId)
    if (googlePolygon) {
      googlePolygon.polygon.polygon.setEditable(false);
      googlePolygon.field.googlePolygon.editable = false;
      let paths = googlePolygon.polygon.getPath().getArray();
      let coordinates = paths.map((coordinate) => [coordinate.lng(), coordinate.lat()]);
      if (!this.isClosedPolygon(coordinates)) {
        coordinates.push(coordinates[0]);
      }
      let turfPolygon = turf.polygon([coordinates]);

      this.store.dispatch(updateGeofence({
        geofence: {
          geofence: turfPolygon.geometry,
          companyFarmId: companyFarmId,
          companyUserId: companyUserId,
          fieldId: fieldId
        }
      }))
    }
  }

  getCurrentPosition(): Observable<GeolocationPosition> {
    return new Observable((observer) => {
      navigator.geolocation.getCurrentPosition(
          (position) => {
            observer.next(position);
            observer.complete();
          },
          (error) => observer.error(error)
      );
    });
  }

  getPolyCenter(polyOptions: google.maps.PolygonOptions): google.maps.LatLng {
    let bounds = new google.maps.LatLngBounds();
    let paths = polyOptions.paths
    paths.forEach(path => bounds.extend(path))
    return bounds.getCenter();
  }

  openInfoWindow(field: MapCompanyField, event: google.maps.PolyMouseEvent) {
    if (!event.vertex) {
      this.closeInfoWindows();
      let foundField = this.fieldInfoWindows.find(fiw => fiw.field.id === field.id);

      if (foundField?.infoWindow) {
        this.selectedField = foundField.field
        foundField.infoWindow.position = event.latLng
        foundField.infoWindow.open();
      }
    }
  }

  closeInfoWindows() {
    this.fieldInfoWindows.forEach(x => x.infoWindow.close())
  }

  getIcon(percents: number, scale: number = 0.25): google.maps.Icon {
    //example for 34
    //leftRatio = 34 % 10 = 4
    //topRatio = Math.floor(34/10) = 3
    //resX = 58 + (228 + 77) * 4 = 1278
    //rexY = 87 + (227+ 77) * 3 = 999

    const xStart = 58
    const yStart = 87
    const gap = 77
    const originalIconWidth = 228;
    const originalIconHeight = 227;
    let imageOriginalSize = {x: 3452, y: 3159}
    let leftRatio = percents % 10
    if (leftRatio === 0 && percents !== 0) {
      leftRatio = 10;
    }
    let topRatio = Math.floor(percents / 10);
    if (topRatio === 10 && percents === 100) {
      topRatio = 9;
    }

    let resX = xStart + (originalIconWidth + gap) * leftRatio
    let rexY = yStart + (originalIconHeight + gap) * topRatio

    return {
      url: '/assets/icon/0-100-stroke.png',
      size: new google.maps.Size(originalIconWidth * scale, originalIconHeight * scale),
      origin: new google.maps.Point(resX * scale, rexY * scale),
      scaledSize: new google.maps.Size(imageOriginalSize.x * scale, imageOriginalSize.y * scale),
      anchor: new google.maps.Point(originalIconWidth * scale / 2, 114 * scale / 2),
    } as google.maps.Icon;
  }

  hideMarkers() {
    let zoom = this.mapRef.googleMap.getZoom();
    if (zoom < 6) {
      this.fieldPolygons.forEach(x => {
        x.polygon.polygon.setVisible(false);
        x.field.googlePolygon.visible = false;
      });
    } else {
      this.fieldPolygons.forEach(x => {
        x.polygon.polygon.setVisible(true);
        x.field.googlePolygon.visible = true;
      });
    }
  }

  onEditPolygon(id: number) {
    let poly = this.fieldPolygons.find(x => x.field.id == id)
    if (poly) {
      poly.polygon.polygon.setEditable(true);
      poly.field.googlePolygon.editable = true;
    }
  }

  getPolygonEditable(fieldId: number): boolean {
    if (this.fieldPolygons.length != 0) {
      let poly = this.fieldPolygons.find(x => x.field.id == fieldId)
      if (poly) {
        return poly.field.googlePolygon.editable;
      }
    }
    return false
  }

  private finishPolygonCreation(polygon: google.maps.Polygon): void {
    let paths = polygon.getPath().getArray();
    let coordinates = paths.map((coordinate) => [coordinate.lng(), coordinate.lat()]);
    coordinates.push(coordinates[0]); // GeoJSON polygons need to be closed rings
    let turfPolygon = turf.polygon([coordinates]);

    polygon.setOptions({draggable: false});
    polygon.setOptions({editable: false});
    polygon.setMap(null);
    this.drawingManager.setDrawingMode(null);
    this.dialog.open(FinishPolygonCreationDialogComponent)
      .afterClosed()
      .pipe(map(data => data as { name: string, farmId: number }))
      .subscribe({
        next: data => {
          if (data != null) {
            this.$companyUser.subscribe(x => {
              this.store.dispatch(createField({
                field: {
                  companyFarmId: data.farmId,
                  name: data.name,
                  companyUserId: x.id,
                  geofence: turfPolygon.geometry
                }
              }))
            })
          } else {
            polygon.setMap(null)
          }
        },
        error: x => {
          polygon.setMap(null)
        }
      });
  }

  private isClosedPolygon(coordinates: number[][]): boolean {
    if (coordinates[0].length != coordinates[coordinates.length -1].length) return false;
    for (let i = 0; i < coordinates[0].length; i++) {
      if (coordinates[0][i] !== coordinates[coordinates.length -1][i]) return false;
    }
    return true;
  }

  private transformToGooglePoly(field: CompanyField, percentage: number): google.maps.PolygonOptions {
    let googleMapsPolygonPaths: google.maps.LatLngLiteral[] = field.geofence.coordinates[0].map((coordinate: any) => {
      return {lat: coordinate[1], lng: coordinate[0]};
    });
    //todo add color gradation by percentage linked to thresholds
    let color = 'rgba(0,0,0,0.54)';
    if (percentage) {
      color = '#2b7714';
      if (percentage <= 20) {
        color = '#cc1212'
      }
      if (percentage > 20 && percentage <= 40) {
        color = '#ce8a25'
      }
    }

    return {
      paths: googleMapsPolygonPaths,
      strokeColor: "rgba(0,0,0,0.54)",
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: color,//check by percentage
      fillOpacity: 0.5,
      clickable: true,
      editable: false
    }
  }

  private findLowestSensorValue(devices: CompanyDevice[]): number {
    let lowest = 100
    for (let device of devices) {
      device.deviceData?.forEach(x => {
        if (x.sensor1 < lowest) lowest = x.sensor1
        if (x.sensor2 < lowest) lowest = x.sensor2
      })
    }
    return lowest
  }

  private getDevicesInField(devices: CompanyDevice[], field: CompanyField) {
    return devices.filter(device => device.fieldId === field.id).map(x => {
      let deviceDataCopy = [...x.deviceData];
      deviceDataCopy.sort((a, b) => new Date(a.timeStamp).getTime() - new Date(b.timeStamp).getTime());
      return { ...x, deviceData: deviceDataCopy };
    });
  }

  private transformField(field: CompanyField, devices: CompanyDevice[]): MapCompanyField {
    const devicesInField = this.getDevicesInField(devices, field);
    const percentage = devicesInField.length !== 0 ? this.findLowestSensorValue(devicesInField) : 0;
    const googlePolygon = this.transformToGooglePoly(field, percentage);
    return {
      companyFarmId: field.companyFarmId,
      devices: devicesInField,
      name: field.name,
      companyId: field.companyId,
      id: field.id,
      percentage: percentage,
      googlePolygon: googlePolygon
    };
  }
}

import {Component, Input} from '@angular/core';
import {MapCompanyField} from "../../../utilities/types/MapTypes";

@Component({
  selector: 'app-map-field-details',
  templateUrl: './map-field-details.component.html',
  styleUrls: ['./map-field-details.component.scss']
})
export class MapFieldDetailsComponent {
  @Input() public field: MapCompanyField
}

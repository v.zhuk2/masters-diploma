import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {CompanyUser, CreateFarm} from "../../../utilities/types/CompanyTypes";
import {Observable, Subscription} from "rxjs";
import {getCurrentCompanyUser} from "../../../store/user-company-user/user-company-user.selectors";

@Component({
  selector: 'app-create-farm-dialog',
  templateUrl: './create-farm-dialog.component.html',
  styleUrls: ['./create-farm-dialog.component.scss'],
})
export class CreateFarmDialogComponent  implements OnInit {
  public nameControl = new FormControl('', [Validators.required]);
  private companyUserSubscription?: Subscription;
  public companyUserId: number
  public companyId: number
  constructor(private readonly dialogRef: MatDialogRef<CreateFarmDialogComponent>,
              private readonly store: Store) { }

  ngOnInit() {
    this.companyUserSubscription = this.store.select(getCurrentCompanyUser).subscribe(x => {
      this.companyUserId = x.id;
      this.companyId = x.companyId;
    });
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    let dialogResult: CreateFarm = {
      name: this.nameControl.value,
      companyId: this.companyId,
      companyUserId: this.companyUserId,
    }
    this.dialogRef.close(dialogResult);
  }

  isValid(): boolean {
    return this.nameControl.valid
  }

  ngOnDestroy() {
    if (this.companyUserSubscription) {
      this.companyUserSubscription.unsubscribe();
    }
  }
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {
  AssignUserToFarm,
  CompanyFarm,
  CompanyFarmRoles,
  CompanyFarmUser,
  CompanyField,
  CompanyUser
} from "../../../utilities/types/CompanyTypes";
import {getFarmById} from "../../../store/farm/farm.selectors";
import {getFieldsByFarmId} from "../../../store/field/field.selectors";
import {getCompanyUsers} from "../../../store/company-user/company-user.selectors";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-farm-details',
  templateUrl: './farm-details.component.html',
  styleUrls: ['./farm-details.component.scss'],
})
export class FarmDetailsComponent implements OnInit {
  id: number;
  $farm: Observable<CompanyFarm>;
  $farmUsers: Observable<CompanyFarmUser[]>;
  $fields: Observable<CompanyField[]>;
  $companyUsers: Observable<CompanyUser[]>
  CompanyFarmRoles = CompanyFarmRoles;

  constructor(private route: ActivatedRoute, private readonly store: Store, private readonly dialog: MatDialog) {
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.$farm = this.store.select(getFarmById(this.id));
    this.$fields = this.store.select(getFieldsByFarmId(this.id));
    this.$companyUsers = this.store.select(getCompanyUsers);
  }
}

import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {Observable} from "rxjs";
import {CompanyFarm, CreateFarm} from "../../utilities/types/CompanyTypes";
import {getFarms} from "../../store/farm/farm.selectors";
import {map} from "rxjs/operators";
import {CreateFarmDialogComponent} from "./create-farm-dialog/create-farm-dialog.component";
import {createFarm} from "../../store/farm/farm.actions";

@Component({
  selector: 'app-company-farms',
  templateUrl: './company-farms.component.html',
  styleUrls: ['./company-farms.component.scss'],
})
export class CompanyFarmsComponent  implements OnInit {
  $farms: Observable<CompanyFarm[]>;

  constructor(private readonly store: Store, private readonly dialog: MatDialog) { }

  ngOnInit() {
    this.$farms = this.store.select(getFarms)
  }

  createFarm() {
    this.dialog.open(CreateFarmDialogComponent, {
      width: '95%'
    }).afterClosed()
      .pipe(map(data => data as CreateFarm))
      .subscribe({
        next: data => {
          if (data != null) {
            this.store.dispatch(createFarm({farm: data}))
          }
        },
        error: x => {
          console.log(x);
        }
      });
  }

}

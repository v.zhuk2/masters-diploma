import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {Company} from "../../utilities/types/CompanyTypes";
import {filter} from "rxjs/operators";
import {Store} from "@ngrx/store";
import {getAppCompanies} from "../../store/app-companies/app-companies.selectors";

@Component({
  selector: 'app-app-companies',
  templateUrl: './app-companies.component.html',
  styleUrls: ['./app-companies.component.scss'],
})
export class AppCompaniesComponent  implements OnInit {
  $companies: Observable<Company[]>;
  constructor(private readonly store: Store) { }

  ngOnInit() {
    this.$companies = this.store.select(getAppCompanies).pipe(
      filter(x => x != null))
  }

}

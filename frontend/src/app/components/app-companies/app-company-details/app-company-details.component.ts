import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {Company} from "../../../utilities/types/CompanyTypes";
import {getAppCompanyById} from "../../../store/app-companies/app-companies.selectors";

@Component({
  selector: 'app-app-company-details',
  templateUrl: './app-company-details.component.html',
  styleUrls: ['./app-company-details.component.scss'],
})
export class AppCompanyDetailsComponent  implements OnInit {
  id: number;
  $company: Observable<Company>
  constructor(private route: ActivatedRoute, private readonly store: Store) { }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.$company = this.store.select(getAppCompanyById(this.id));
  }

}

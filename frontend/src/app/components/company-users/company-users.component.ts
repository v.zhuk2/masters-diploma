import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {CompanyRoles, CompanyUser, CreateFarm, InviteUser} from "../../utilities/types/CompanyTypes";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {getCompanyUsers} from "../../store/company-user/company-user.selectors";
import {filter, map} from "rxjs/operators";
import {InviteUserDialogComponent} from "./invite-user-dialog/invite-user-dialog.component";
import {inviteUser} from "../../store/company-user/company-user.actions";

@Component({
  selector: 'app-company-users',
  templateUrl: './company-users.component.html',
  styleUrls: ['./company-users.component.scss'],
})
export class CompanyUsersComponent  implements OnInit {
  $users: Observable<CompanyUser[]>;
  CompanyRoles = CompanyRoles
  constructor(private readonly store: Store, private readonly dialog: MatDialog) { }

  ngOnInit() {
    this.$users = this.store.select(getCompanyUsers).pipe(filter(x => x != null))

  }

  inviteUser() {
    this.dialog.open(InviteUserDialogComponent, {
      width: '95%'
    }).afterClosed()
      .pipe(map(data => data as InviteUser))
      .subscribe({
        next: data => {
          if (data != null) {
            this.store.dispatch(inviteUser({user: data}))
          }
        },
        error: x => {
          console.log(x);
        }
      });
  }

}

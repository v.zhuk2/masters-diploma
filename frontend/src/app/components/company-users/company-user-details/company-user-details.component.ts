import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {getCompanyUserById} from "../../../store/company-user/company-user.selectors";
import {Observable} from "rxjs";
import {
  CompanyFarm,
  CompanyFarmRoles,
  CompanyFarmUser,
  CompanyRoles,
  CompanyUser
} from "../../../utilities/types/CompanyTypes";
import {getFarms} from "../../../store/farm/farm.selectors";

@Component({
  selector: 'app-company-user-details',
  templateUrl: './company-user-details.component.html',
  styleUrls: ['./company-user-details.component.scss'],
})
export class CompanyUserDetailsComponent implements OnInit {
  id: number;
  $user: Observable<CompanyUser>;
  $farms: Observable<CompanyFarm[]>;
  $farmUsers: Observable<CompanyFarmUser[]>
  CompanyFarmRoles = CompanyFarmRoles
  CompanyRoles = CompanyRoles

  constructor(private route: ActivatedRoute, private readonly store: Store) {
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.$user = this.store.select(getCompanyUserById(this.id));
    this.$farms = this.store.select(getFarms);
  }

  getFarmName(companyFarms: CompanyFarm[], farmId: number): string {
    return companyFarms.find(x => x.id == farmId).name
  }

}

import {Component, OnInit} from '@angular/core';
import {CompanyRoles, InviteUser} from "../../../utilities/types/CompanyTypes";
import {getCurrentCompanyUser} from "../../../store/user-company-user/user-company-user.selectors";
import {MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {FormControl, Validators} from "@angular/forms";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-invite-user-dialog',
  templateUrl: './invite-user-dialog.component.html',
  styleUrls: ['./invite-user-dialog.component.scss'],
})
export class InviteUserDialogComponent implements OnInit {
  public emailControl = new FormControl('', [Validators.required]);
  public roleControl = new FormControl(CompanyRoles.CompanyUser, [Validators.required]);
  private companyUserSubscription?: Subscription;
  public companyUserId: number
  public companyId: number
  CompanyRoles = CompanyRoles

  constructor(private readonly dialogRef: MatDialogRef<InviteUserDialogComponent>,
              private readonly store: Store) {
  }

  ngOnInit() {
    this.companyUserSubscription = this.store.select(getCurrentCompanyUser).subscribe(x => {
      this.companyUserId = x.id;
      this.companyId = x.companyId;
    });
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    let dialogResult: InviteUser = {
      email: this.emailControl.value,
      companyRole: this.roleControl.value,
      companyId: this.companyId,
      companyUserId: this.companyUserId,
    }
    this.dialogRef.close(dialogResult);
  }

  isValid(): boolean {
    return this.emailControl.valid && this.roleControl.valid
  }

  ngOnDestroy() {
    if (this.companyUserSubscription) {
      this.companyUserSubscription.unsubscribe();
    }
  }

}

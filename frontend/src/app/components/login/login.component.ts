import {AfterViewInit, Component} from '@angular/core';
import {AuthService} from '../../services/api-services/auth-service';
import {FormControl, } from '@angular/forms';
import {UserLogin} from '../../utilities/types/auth-types';
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements AfterViewInit {
    ngAfterViewInit(): void {
        document.querySelector('body').click();
    }

    public email = new FormControl('');
    public password = new FormControl('');
    public loginResult = '';

    constructor(
        private router: Router,
        private readonly authService: AuthService,
    ) {
    }

    login() {
        if (this.email.value && this.password.value) {
            let credentials: UserLogin = {
                email: this.email.value,
                password: this.password.value,
            }
            this.authService.login(credentials).subscribe({
                next: () => {
                    this.loginResult = '';
                    this.router.navigateByUrl('/home');
                },
                error: () => {
                    this.loginResult = 'Wrong email or password';
                }
            })
        }
    }

    loginValid(): boolean {
        return this.password.valid && this.email.valid
    }
}




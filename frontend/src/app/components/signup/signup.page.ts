import {Component} from '@angular/core';
import {AbstractControl, FormControl, ValidationErrors, ValidatorFn, Validators} from "@angular/forms";
import {AuthService} from "../../services/api-services/auth-service";
import {UserRegistration} from "../../utilities/types/auth-types";
import {Router} from "@angular/router";

export function passwordMatchValidator(passwordControl: AbstractControl): ValidatorFn {
    return (repeatPasswordControl: AbstractControl): ValidationErrors | null => {
        if (!passwordControl || !repeatPasswordControl) return null;
        if (passwordControl.value !== repeatPasswordControl.value) return {passwordMismatch: true};
        return null;
    };
}

@Component({
    selector: 'app-signup',
    templateUrl: './signup.page.html',
    styleUrls: ['./signup.page.scss'],
})
export class SignupPage {
    public email = new FormControl('', [Validators.required])
    public firstname = new FormControl('', [Validators.required])
    public lastname = new FormControl('', [Validators.required])
    public password = new FormControl('', [Validators.required])
    public repeatPassword = new FormControl('', [Validators.required, passwordMatchValidator(this.password)])
    public registrationResult = '';

    constructor(private readonly router: Router, private readonly authService: AuthService) {
    }

    register() {
        let userData: UserRegistration = {
            email: this.email.value,
            firstName: this.firstname.value,
            lastName: this.lastname.value,
            password: this.password.value
        }

        this.authService.register(userData).subscribe({
            next: () => {
              this.router.navigateByUrl('/auth/registration-successful')
            }, error: (error) => this.registrationResult = error.message
        })
    }

    registrationValid(): boolean {
        return this.email.valid && this.firstname.valid && this.lastname.valid && this.passwordValid()
    }

    passwordValid(): boolean {
        return this.password.value == this.repeatPassword.value && this.password.valid && this.repeatPassword.valid
    }
}

import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-reg-successful',
  templateUrl: './reg-successful.component.html',
  styleUrls: ['./reg-successful.component.scss'],
})
export class RegSuccessfulComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {}


  goToLogin(): void {
    this.router.navigateByUrl('/auth/login')
  }
}

import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {Company} from "../../utilities/types/CompanyTypes";
import {getCompany} from "../../store/company/company.selectors";
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-user-company',
  templateUrl: './user-company.component.html',
  styleUrls: ['./user-company.component.scss'],
})
export class UserCompanyComponent  implements OnInit {
  id: number;
  $company: Observable<Company>
  constructor(private route: ActivatedRoute, private readonly store: Store) { }

  ngOnInit() {
    this.$company = this.store.select(getCompany).pipe(filter(x => !!x))
  }

}

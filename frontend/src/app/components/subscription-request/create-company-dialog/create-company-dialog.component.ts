import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import { CreateCompany} from "../../../utilities/types/CompanyTypes";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-create-company-dialog',
  templateUrl: './create-company-dialog.component.html',
  styleUrls: ['./create-company-dialog.component.scss'],
})
export class CreateCompanyDialogComponent implements OnInit {
  public nameControl = new FormControl('', [Validators.required]);
  public emailControl = new FormControl('', [Validators.required]);
  public maxDevicesControl = new FormControl(10, [Validators.required]);
  constructor(private readonly dialogRef: MatDialogRef<CreateCompanyDialogComponent>) {
  }

  ngOnInit() {
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    let dialogResult: CreateCompany = {
      name: this.nameControl.value,
      maxDevices: this.maxDevicesControl.value,
      email: this.emailControl.value

    }
    this.dialogRef.close(dialogResult);
  }

  isValid(): boolean {
    return this.nameControl.valid && this.emailControl.valid && this.maxDevicesControl.valid
  }

}

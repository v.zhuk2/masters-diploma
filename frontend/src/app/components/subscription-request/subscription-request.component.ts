import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {CreateCompany, SubscriptionRequest} from "../../utilities/types/CompanyTypes";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {filter, map} from "rxjs/operators";
import {getSubscriptionRequests} from "../../store/subscription-request/subscription-request.selectors";
import {CreateCompanyDialogComponent} from "./create-company-dialog/create-company-dialog.component";
import {createAppCompany} from "../../store/app-companies/app-companies.actions";

@Component({
  selector: 'app-subscription-request',
  templateUrl: './subscription-request.component.html',
  styleUrls: ['./subscription-request.component.scss'],
})
export class SubscriptionRequestComponent implements OnInit {
  $subscriptions: Observable<SubscriptionRequest[]>;

  constructor(private readonly store: Store, private readonly dialog: MatDialog) {
  }

  ngOnInit() {
    this.$subscriptions = this.store.select(getSubscriptionRequests).pipe(
      filter(x => x != null),
      map(x => x.filter(z => !z.isFulfilled))
    )
  }

  createSubscription() {
    this.dialog.open(CreateCompanyDialogComponent, {
      width: '95%'
    }).afterClosed()
      .pipe(map(data => data as CreateCompany))
      .subscribe({
        next: data => {
          if (data != null) {
            this.store.dispatch(createAppCompany({company: data}))
          }
        }, error: x => {
          console.log(x);
        }
      });
  }
}

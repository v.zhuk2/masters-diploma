import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {SubscriptionRequest} from "../../../utilities/types/CompanyTypes";
import {getSubscriptionRequestById,} from "../../../store/subscription-request/subscription-request.selectors";
import {createAppCompany} from "../../../store/app-companies/app-companies.actions";

@Component({
  selector: 'app-subscription-request-details',
  templateUrl: './subscription-request-details.component.html',
  styleUrls: ['./subscription-request-details.component.scss'],
})
export class SubscriptionRequestDetailsComponent implements OnInit {
  id: number;
  $request: Observable<SubscriptionRequest>

  constructor(private route: ActivatedRoute, private readonly store: Store) {
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.$request = this.store.select(getSubscriptionRequestById(this.id))
  }

  fulfillRequest(request: SubscriptionRequest) {
    this.store.dispatch(createAppCompany({
      company: {
        name: request.companyName,
        email: request.email,
        maxDevices: request.maxDevices
      }
    }))
  }

}

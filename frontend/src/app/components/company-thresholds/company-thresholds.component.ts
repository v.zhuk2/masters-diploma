import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {Observable} from "rxjs";
import {CompanyThresholdPreset, CreatePreset} from "../../utilities/types/CompanyTypes";

import {getThresholdPresets} from "../../store/threshold-preset/threshold-preset.selectors";
import {map} from "rxjs/operators";
import {CreateThresholdDialogComponent} from "./create-threshold-dialog/create-threshold-dialog.component";
import {createPreset} from "../../store/threshold-preset/threshold-preset.actions";

@Component({
  selector: 'app-company-thresholds',
  templateUrl: './company-thresholds.component.html',
  styleUrls: ['./company-thresholds.component.scss'],
})
export class CompanyThresholdsComponent  implements OnInit {
  $presets: Observable<CompanyThresholdPreset[]>;
  constructor(private readonly store: Store, private readonly dialog: MatDialog) { }

  ngOnInit() {
    this.$presets = this.store.select(getThresholdPresets)
  }

  createPreset() {
    this.dialog.open(CreateThresholdDialogComponent, {
      width: '95%'
    }).afterClosed()
      .pipe(map(data => data as CreatePreset))
      .subscribe({
        next: data => {
          if (data != null) {
            this.store.dispatch(createPreset({preset: data}))
          }
        },
        error: x => {
          console.log(x);
        }
      });
  }
}

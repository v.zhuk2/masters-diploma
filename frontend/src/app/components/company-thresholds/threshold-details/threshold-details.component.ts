import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {CompanyThresholdPreset} from "../../../utilities/types/CompanyTypes";
import {ActivatedRoute} from "@angular/router";
import {Store} from "@ngrx/store";
import {getThresholdPresetById} from "../../../store/threshold-preset/threshold-preset.selectors";

@Component({
  selector: 'app-threshold-details',
  templateUrl: './threshold-details.component.html',
  styleUrls: ['./threshold-details.component.scss'],
})
export class ThresholdDetailsComponent  implements OnInit {
  id: number;
  $preset: Observable<CompanyThresholdPreset>

  constructor(private route: ActivatedRoute, private readonly store: Store) {
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.$preset = this.store.select(getThresholdPresetById(this.id))
  }

}

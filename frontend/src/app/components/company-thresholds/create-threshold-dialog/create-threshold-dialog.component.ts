import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {CreatePreset} from "../../../utilities/types/CompanyTypes";
import {MatDialogRef} from "@angular/material/dialog";
import {Store} from "@ngrx/store";
import {getCurrentCompanyUser} from "../../../store/user-company-user/user-company-user.selectors";

@Component({
  selector: 'app-create-threshold-dialog',
  templateUrl: './create-threshold-dialog.component.html',
  styleUrls: ['./create-threshold-dialog.component.scss'],
})
export class CreateThresholdDialogComponent  implements OnInit {
  public nameControl = new FormControl('', [Validators.required]);
  public DefaultHumidity1Min = new FormControl(1, [Validators.required]);
  public DefaultHumidity1Max = new FormControl(100, [Validators.required]);
  public DefaultHumidity2Min = new FormControl(1, [Validators.required]);
  public DefaultHumidity2Max = new FormControl(100, [Validators.required]);
  public DefaultBatteryLevelMin = new FormControl(1, [Validators.required]);
  public DefaultBatteryLevelMax = new FormControl(100, [Validators.required]);
  public DefaultTemperatureMin = new FormControl(-20, [Validators.required]);
  public DefaultTemperatureMax = new FormControl(30, [Validators.required]);
  public min = 1;
  public max = 100;
  public companyUserId: number
  public companyId: number
  private companyUserSubscription?: Subscription;


  constructor(private readonly dialogRef: MatDialogRef<CreateThresholdDialogComponent>,
              private readonly store: Store) { }

  ngOnInit() {
    this.companyUserSubscription = this.store.select(getCurrentCompanyUser).subscribe(x => {
      this.companyUserId = x.id;
      this.companyId = x.companyId;
    });
  }

  onClose(): void {
    this.dialogRef.close(null);
  }

  onSave(): void {
    let dialogResult: CreatePreset = {
      name: this.nameControl.value,
      companyId: this.companyId,
      companyUserId: this.companyUserId,
      defaultHumidity1Min: this.DefaultHumidity1Min.value,
      defaultHumidity1Max: this.DefaultHumidity1Max.value,
      defaultHumidity2Min: this.DefaultHumidity2Min.value,
      defaultHumidity2Max: this.DefaultHumidity2Max.value,
      defaultBatteryLevelMin: this.DefaultBatteryLevelMin.value,
      defaultBatteryLevelMax: this.DefaultBatteryLevelMax.value,
      defaultTemperatureMin: this.DefaultTemperatureMin.value,
      defaultTemperatureMax: this.DefaultTemperatureMax.value,
    }
    this.dialogRef.close(dialogResult);
  }

  isValid(): boolean {
    return this.nameControl.valid &&
      this.DefaultHumidity1Min.valid &&
      this.DefaultHumidity1Max.valid &&
      this.DefaultHumidity2Min.valid &&
      this.DefaultHumidity2Max.valid &&
      this.DefaultBatteryLevelMin.valid &&
      this.DefaultBatteryLevelMax.valid &&
      this.DefaultTemperatureMin.valid &&
      this.DefaultTemperatureMax.valid;
  }

  ngOnDestroy() {
    if (this.companyUserSubscription) {
      this.companyUserSubscription.unsubscribe();
    }
  }


}

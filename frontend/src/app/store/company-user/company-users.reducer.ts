import {createReducer, on} from '@ngrx/store';
import {CompanyUserState} from '../State';
import {
  companyUserLogout,
  initLoadCompanyUsers,
  initLoadCompanyUsersFailure,
  initLoadCompanyUsersSuccess, inviteUser, inviteUserFailure, inviteUserSuccess
} from "./company-user.actions";
import {createFarm, createFarmFailure, createFarmSuccess} from "../farm/farm.actions";

export const InitialCompanyUserState: CompanyUserState = {
  companyUsers: null,
  error: null,
  isLoading: false,
};

export const CompanyUsersReducer = createReducer(
  InitialCompanyUserState,
  on(initLoadCompanyUsers, state => ({ ...state, isLoading: true})),
  on(initLoadCompanyUsersSuccess, (state, {companyUsers}) => ({
    ...state, companyUsers
  })),
  on(initLoadCompanyUsersFailure, (state, {error}) => ({
    ...state, error
  })),
  on(inviteUser, state => {
    return {...state, isLoading: true};
  }),
  on(inviteUserSuccess, (state, {companyUser}) => {
    const updatedCompanyUsers = [...state.companyUsers, companyUser];
    return {
      ...state, companyUsers: updatedCompanyUsers, isLoading: false
    }
  }),
  on(inviteUserFailure, (state, {error}) => {
    return {...state, error, isLoading: false};
  }),
  on(companyUserLogout, state => {
    return InitialCompanyUserState
  })
);

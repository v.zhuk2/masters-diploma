import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {CompanyUser, InviteUser} from "../../utilities/types/CompanyTypes";

@Injectable({
  providedIn: 'root'
})
export class CompanyUserService {

  constructor(private http: HttpClient) {}

  private controllerUrl = environment.apiBaseUrl + '/CompanyUser';

  public getUserCompanyUsers(): Observable<CompanyUser[]> {
    return this.http.get<CompanyUser[]>(this.controllerUrl + '/getUserCompanyUsers');
  }

  public getCompanyUsers(companyUserId: number): Observable<CompanyUser[]> {
    return this.http.post<CompanyUser[]>(this.controllerUrl + '/getCompanyUsers', companyUserId);
  }

  public inviteUser(user: InviteUser): Observable<CompanyUser> {
    return this.http.post<CompanyUser>(this.controllerUrl + '/inviteUser', user);
  }

}

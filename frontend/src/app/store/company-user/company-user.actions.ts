import {createAction, props} from "@ngrx/store";
import {CompanyUser, InviteUser} from "../../utilities/types/CompanyTypes";

export const initLoadCompanyUsers = createAction('[CompanyUsers] Init Load CompanyUsers',
  props<{ companyUserId: number }>());
export const initLoadCompanyUsersSuccess = createAction(
  '[CompanyUsers] Init Load CompanyUsers Success',
  props<{ companyUsers: CompanyUser[] }>()
);
export const initLoadCompanyUsersFailure = createAction(
  '[CompanyUsers] Init Load CompanyUsers Failure',
  props<{ error: string | null }>()
);

export const companyUserLogout = createAction('[CompanyUsers] Logout');
export const inviteUser = createAction('[CompanyUsers] Invite CompanyUsers', props<{ user: InviteUser }>())
export const inviteUserSuccess = createAction(
  '[CompanyUsers] Invite CompanyUsers Success',
  props<{ companyUser: CompanyUser }>())
export const inviteUserFailure = createAction(
  '[CompanyUsers] Invite CompanyUsers Failure',
  props<{ error: string }>())

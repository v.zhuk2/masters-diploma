import {createFeatureSelector, createSelector} from '@ngrx/store';
import {CompanyUserState} from '../State'

export const getCompanyUsersState = createFeatureSelector<CompanyUserState>('companyUsers');

export const getCompanyUsers = createSelector(
  getCompanyUsersState,
  (state: CompanyUserState) => state?.companyUsers
);

export const isLoadingCompanyUsers = createSelector(
  getCompanyUsersState,
  (state: CompanyUserState) => state?.isLoading ?? true
);

export const companyUsersError = createSelector(
  getCompanyUsersState,
  (state: CompanyUserState) => state?.error
);


export const getCompanyUserById = (id: number) => createSelector(
  getCompanyUsersState,
  (state: CompanyUserState) => state?.companyUsers?.find(cu => cu.id === id)
);

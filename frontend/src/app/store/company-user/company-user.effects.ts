import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {
  initLoadCompanyUsers,
  initLoadCompanyUsersFailure,
  initLoadCompanyUsersSuccess,
  inviteUser, inviteUserFailure, inviteUserSuccess
} from "./company-user.actions";
import {CompanyUserService} from "./company-user.service";
import {SnackBarService} from "../../services/snack-bar.service";

@Injectable()
export class CompanyUserEffects {
  loadCompanyUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadCompanyUsers),
      mergeMap(({companyUserId}) =>
        this.companyUserService.getCompanyUsers(companyUserId).pipe(
          map(companyUsers => initLoadCompanyUsersSuccess({companyUsers: companyUsers})
          ),
          catchError((error) =>
            of(initLoadCompanyUsersFailure({error: error})))
        )
      )
    )
  );

  //todo here an example of error displaying
  inviteUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(inviteUser),
      mergeMap((data) =>
        this.companyUserService.inviteUser(data.user).pipe(
          map(user => inviteUserSuccess({companyUser: user})),
          catchError((error) => {
            this.snackBarService.error(error.error)
            return of(inviteUserFailure({error: error}));
          })
        )
      )
    )
  );

  constructor(private actions$: Actions,
              private companyUserService: CompanyUserService,
              private readonly snackBarService: SnackBarService) {
  }
}

import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {CompanyService} from "../company/company.service";
import {
  initLoadSubscriptionRequests,
  initLoadSubscriptionRequestsFailure,
  initLoadSubscriptionRequestsSuccess
} from "./subscription-request.actions";

@Injectable()
export class SubscriptionRequestEffects {

  loadSubscriptionRequests$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadSubscriptionRequests),
      mergeMap(() =>
        this.companyService.getSubRequests().pipe(
          map(subscriptionRequests => initLoadSubscriptionRequestsSuccess({subscriptionRequests: subscriptionRequests})),
          catchError((error) => of(initLoadSubscriptionRequestsFailure({
            error: error
          })))
        )
      )
    )
  );

  constructor(private actions$: Actions,
              private companyService: CompanyService) {
  }
}

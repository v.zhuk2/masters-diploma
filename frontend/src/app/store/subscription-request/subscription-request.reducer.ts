import { createReducer, on } from '@ngrx/store';
import {SubscriptionRequestState} from '../State';
import {
  initLoadSubscriptionRequests,
  initLoadSubscriptionRequestsFailure,
  initLoadSubscriptionRequestsSuccess, subscriptionRequestsLogout
} from "./subscription-request.actions";

export const InitialSubscriptionRequestsState: SubscriptionRequestState = {
  subscriptionRequests: null,
  error: null,
  isLoading: true,
};

export const SubscriptionRequestsReducer = createReducer(
  InitialSubscriptionRequestsState,
  on(initLoadSubscriptionRequests, state => state),
  on(initLoadSubscriptionRequestsSuccess, (state, {subscriptionRequests}) => ({
    ...state, subscriptionRequests, error: null, isLoading: false
  })),
  on(initLoadSubscriptionRequestsFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(subscriptionRequestsLogout, state => {
    return InitialSubscriptionRequestsState
  })
);

import {createAction, props} from "@ngrx/store";
import {SubscriptionRequest} from "../../utilities/types/CompanyTypes";

export const initLoadSubscriptionRequests = createAction('[SubscriptionRequests] Init Load SubscriptionRequests');
export const initLoadSubscriptionRequestsSuccess = createAction(
  '[SubscriptionRequests] Init Load SubscriptionRequests Success',
  props<{ subscriptionRequests: SubscriptionRequest[] }>()
);
export const initLoadSubscriptionRequestsFailure = createAction(
  '[SubscriptionRequests] Init Load SubscriptionRequests Failure',
  props<{ error: string | null}>()
);
export const subscriptionRequestsLogout = createAction('[SubscriptionRequests] Logout');

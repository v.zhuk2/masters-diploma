import { createFeatureSelector, createSelector } from '@ngrx/store';
import {AppCompanyState, FarmState, SubscriptionRequestState} from '../State'
import {getFarmsState} from "../farm/farm.selectors";
export const getSubscriptionRequestState = createFeatureSelector<SubscriptionRequestState>('subscriptionRequests');

export const getSubscriptionRequests = createSelector(
  getSubscriptionRequestState,
  (state: SubscriptionRequestState) => state?.subscriptionRequests
);

export const isSubscriptionRequests = createSelector(
  getSubscriptionRequestState,
  (state: SubscriptionRequestState) => state?.isLoading
);

export const subscriptionRequestsError = createSelector(
  getSubscriptionRequestState,
  (state: SubscriptionRequestState) => state?.error
);

export const getSubscriptionRequestById = (id: number) => createSelector(
  getSubscriptionRequestState,
  (state: SubscriptionRequestState) => state?.subscriptionRequests?.find(sr => sr.id === id)
);

import {createReducer, on} from '@ngrx/store';
import {UserCompanyUserState} from '../State';
import {
  changeCurrentUser,
  initLoadUserCompanyUsers,
  initLoadUserCompanyUsersFailure,
  initLoadUserCompanyUsersSuccess,
  userCompanyUserLogout
} from "./user-company-user.actions";

export const InitialUserCompanyUserState: UserCompanyUserState = {
  companyUsers: null,
  currentCompanyUser: null,
  error: null,
  isLoading: false,
};

export const UserCompanyUsersReducer = createReducer(
  InitialUserCompanyUserState,
  on(initLoadUserCompanyUsers, state => ({...state, isLoading: true})),
  on(initLoadUserCompanyUsersSuccess, (state, {userCompanyUsers, currentCompanyUser}) => ({
    ...state, companyUsers: userCompanyUsers, currentCompanyUser: currentCompanyUser, error: null, isLoading: false
  })),
  on(initLoadUserCompanyUsersFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(changeCurrentUser, (state, {currentUser}) => ({ ...state, currentCompanyUser: currentUser})),
  on(userCompanyUserLogout, state => {
    return InitialUserCompanyUserState
  })
);

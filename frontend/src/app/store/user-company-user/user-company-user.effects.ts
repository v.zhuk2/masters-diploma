import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {initLoadFarms} from "../farm/farm.actions";
import {initLoadFields} from "../field/field.actions";
import {initLoadDevices} from "../device/device.actions";
import {initLoadThresholdPresets} from "../threshold-preset/threshold-preset.actions";
import {initLoadDeviceData} from "../device-data/device-data.actions";
import {CompanyRoles} from "../../utilities/types/CompanyTypes";
import {initLoadCompany} from "../company/company.actions";
import {Action} from "@ngrx/store";
import {
  changeCurrentUser,
  initLoadUserCompanyUsers,
  initLoadUserCompanyUsersFailure,
  initLoadUserCompanyUsersSuccess
} from "./user-company-user.actions";
import {CompanyUserService} from "../company-user/company-user.service";
import {initLoadCompanyUsers} from "../company-user/company-user.actions";

@Injectable()
export class UserCompanyUserEffects {

  loadUserCompanyUsers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadUserCompanyUsers),
      mergeMap(() =>
        this.companyUserService.getUserCompanyUsers().pipe(
          mergeMap(userCompanyUsers => {
              let actions: Action[] = []
              if (userCompanyUsers != null) {
                let defaultCompanyUser = userCompanyUsers.find(x => x.isDefault)

                if (defaultCompanyUser != null) {
                  switch (defaultCompanyUser.companyRole) {
                    case CompanyRoles.CompanyOwner: {
                      actions.push(initLoadCompany({companyUserId: defaultCompanyUser.id}))
                      actions.push(initLoadCompanyUsers({companyUserId: defaultCompanyUser.id}))
                      break;
                    }
                    case CompanyRoles.CompanyAdmin: {
                      actions.push(initLoadCompanyUsers({companyUserId: defaultCompanyUser.id}))
                      break;
                    }
                    default:
                      break;
                  }
                  actions = actions.concat([
                    initLoadUserCompanyUsersSuccess({
                      userCompanyUsers: userCompanyUsers,
                      currentCompanyUser: defaultCompanyUser
                    }),
                    initLoadDevices({companyUserId: defaultCompanyUser.id}),
                    initLoadFields({companyUserId: defaultCompanyUser.id}),
                    initLoadFarms({companyUserId: defaultCompanyUser.id}),
                    initLoadThresholdPresets({companyId: defaultCompanyUser.companyId}),
                    initLoadDeviceData({companyUserId: defaultCompanyUser.id})
                  ]);
                }
              }

              return actions;
            }
          ),
          catchError((error) =>
            of(initLoadUserCompanyUsersFailure({error: error})))
        )
      )
    )
  );

  changeCurrentUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(changeCurrentUser),
      mergeMap(({currentUser}) => {
          let actions: Action[] = []
          switch (currentUser.companyRole) {
            case CompanyRoles.CompanyOwner: {
              actions.push(initLoadCompany({companyUserId: currentUser.id}))
              actions.push(initLoadCompanyUsers({companyUserId: currentUser.id}))
              break;
            }
            case CompanyRoles.CompanyAdmin: {
              actions.push(initLoadCompanyUsers({companyUserId: currentUser.id}))
              break;
            }
            default:
              break;
          }
          actions = actions.concat([
            initLoadDevices({companyUserId: currentUser.id}),
            initLoadFields({companyUserId: currentUser.id}),
            initLoadFarms({companyUserId: currentUser.id}),
            initLoadThresholdPresets({companyId: currentUser.companyId}),
            initLoadDeviceData({companyUserId: currentUser.id})
          ]);
          return actions;
        }
      )
    )
  );

  constructor(private actions$: Actions,
              private companyUserService: CompanyUserService) {
  }
}

import {createFeatureSelector, createSelector} from '@ngrx/store';
import {CompanyUserState, UserCompanyUserState} from '../State'

export const getUserCompanyUsersState = createFeatureSelector<UserCompanyUserState>('userCompanyUsers');

export const getUserCompanyUsers = createSelector(
  getUserCompanyUsersState,
  (state: UserCompanyUserState) => state?.companyUsers
);

export const getCurrentCompanyUser = createSelector(
  getUserCompanyUsersState,
  (state: UserCompanyUserState) => state?.currentCompanyUser
);

export const isLoadingUserCompanyUsers = createSelector(
  getUserCompanyUsersState,
  (state: UserCompanyUserState) => state?.isLoading ?? true
);

export const userCompanyUsersError = createSelector(
  getUserCompanyUsersState,
  (state: UserCompanyUserState) => state?.error
);


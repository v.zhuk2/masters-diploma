import {createAction, props} from "@ngrx/store";
import {CompanyUser} from "../../utilities/types/CompanyTypes";

export const initLoadUserCompanyUsers = createAction('[UserCompanyUsers] Init Load UserCompanyUsers');
export const initLoadUserCompanyUsersSuccess = createAction(
  '[UserCompanyUsers] Init Load UserCompanyUsers Success',
  props<{ userCompanyUsers: CompanyUser[], currentCompanyUser: CompanyUser }>()
);
export const initLoadUserCompanyUsersFailure = createAction(
  '[UserCompanyUsers] Init Load UserCompanyUsers Failure',
  props<{ error: string | null}>()
);

export const userCompanyUserLogout = createAction('[UserCompanyUsers] Logout');

export const changeCurrentUser = createAction('[UserCompanyUsers] Change Current User', props<{currentUser: CompanyUser}>())

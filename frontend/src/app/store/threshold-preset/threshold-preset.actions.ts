import {createAction, props} from "@ngrx/store";
import {CompanyThresholdPreset, CreatePreset} from "../../utilities/types/CompanyTypes";

export const initLoadThresholdPresets = createAction('[ThresholdPresets] Init Load ThresholdPresets', props<{ companyId: number }>());

export const initLoadThresholdPresetsSuccess = createAction('[ThresholdPresets] Init Load ThresholdPresets Success', props<{ thresholdPresets: CompanyThresholdPreset[] }>());

export const initLoadThresholdPresetsFailure = createAction('[ThresholdPresets] Init Load ThresholdPresets Failure', props<{ error: string | null }>());
export const thresholdPresetsLogout = createAction('[ThresholdPresets] Logout');

export const createPreset = createAction(
  '[ThresholdPresets] Create ThresholdPreset',
  props<{ preset: CreatePreset }>())
export const createPresetSuccess = createAction(
  '[ThresholdPresets] Create ThresholdPreset Success',
  props<{ preset: CompanyThresholdPreset }>())
export const createPresetFailure = createAction(
  '[ThresholdPresets] Create ThresholdPreset Failure',
  props<{ error: string }>())

import {createFeatureSelector, createSelector} from '@ngrx/store';
import {FarmState, ThresholdPresetsState} from '../State'
import {getFarmsState} from "../farm/farm.selectors";

export const getThresholdPresetsState = createFeatureSelector<ThresholdPresetsState>('thresholdPresets');

export const getThresholdPresets =
  createSelector(getThresholdPresetsState, (state: ThresholdPresetsState) => state?.thresholdPresets);

export const isLoadingThresholdPresets =
  createSelector(getThresholdPresetsState, (state: ThresholdPresetsState) => state?.isLoading);

export const thresholdPresetsError =
  createSelector(getThresholdPresetsState, (state: ThresholdPresetsState) => state?.error);

export const getThresholdPresetById = (id: number) => createSelector(
  getThresholdPresetsState,
  (state: ThresholdPresetsState) => state?.thresholdPresets?.find(preset => preset.id === id)
);

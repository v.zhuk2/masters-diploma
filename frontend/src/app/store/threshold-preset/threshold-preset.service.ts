import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {CompanyThresholdPreset, CreatePreset,} from "../../utilities/types/CompanyTypes";

@Injectable({
  providedIn: 'root'
})
export class ThresholdPresetService {

  constructor(private http: HttpClient) {
  }

  private controllerUrl = environment.apiBaseUrl + '/ThresholdPreset';

  public createThresholdPreset(preset: CreatePreset): Observable<CompanyThresholdPreset> {
    return this.http.post<CompanyThresholdPreset>(this.controllerUrl + '/createThresholdPreset', preset);
  }

  public getThresholdPresets(companyUserId: number): Observable<CompanyThresholdPreset[]> {
    return this.http.post<CompanyThresholdPreset[]>(this.controllerUrl + '/getThresholdPresets', companyUserId);
  }
}

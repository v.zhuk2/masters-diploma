import {createReducer, on} from '@ngrx/store';
import {ThresholdPresetsState} from '../State';
import {
  createPreset, createPresetFailure, createPresetSuccess,
  initLoadThresholdPresets,
  initLoadThresholdPresetsFailure,
  initLoadThresholdPresetsSuccess, thresholdPresetsLogout
} from "./threshold-preset.actions";
import {createFarm, createFarmFailure, createFarmSuccess} from "../farm/farm.actions";

export const InitialThresholdPresetsState: ThresholdPresetsState = {
  thresholdPresets: null,
  error: null,
  isLoading: true,
};

export const ThresholdPresetsReducer = createReducer(
  InitialThresholdPresetsState,
  on(initLoadThresholdPresets, state => state),
  on(initLoadThresholdPresetsSuccess, (state, {thresholdPresets}) => ({
    ...state, thresholdPresets, error: null, isLoading: false
  })),
  on(initLoadThresholdPresetsFailure, (state, { error}) => ({
    ...state, error, isLoading: true
  })),
  on(createPreset, state => {
    return {...state, isLoading: true};
  }),
  on(createPresetSuccess, (state, {preset}) => {
    const updatedPresets = [...state.thresholdPresets, preset];
    return {
      ...state, thresholdPresets: updatedPresets, isLoading: false
    }
  }),
  on(createPresetFailure, (state, {error}) => {
    return {...state, error, isLoading: false};
  }),
  on(thresholdPresetsLogout, state => {
    return InitialThresholdPresetsState
  })
);

import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {
  createPreset, createPresetFailure, createPresetSuccess,
  initLoadThresholdPresets,
  initLoadThresholdPresetsFailure,
  initLoadThresholdPresetsSuccess
} from "./threshold-preset.actions";
import {ThresholdPresetService} from "./threshold-preset.service";
import {createFarm, createFarmFailure, createFarmSuccess} from "../farm/farm.actions";

@Injectable()
export class ThresholdPresetEffects {

  loadThresholdPresets$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadThresholdPresets),
      mergeMap(({companyId}) =>
        this.thresholdPresetService.getThresholdPresets(companyId).pipe(
          map(thresholdPresets =>
            initLoadThresholdPresetsSuccess({thresholdPresets: thresholdPresets})),
          catchError((error) =>
            of(initLoadThresholdPresetsFailure({error: error})))
        )
      )
    )
  );

  createPreset$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createPreset),
      mergeMap((data) =>
        this.thresholdPresetService.createThresholdPreset(data.preset).pipe(
          map(preset => createPresetSuccess({preset: preset})),
          catchError((error) =>
            of(createPresetFailure({error: error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private thresholdPresetService: ThresholdPresetService) {}
}

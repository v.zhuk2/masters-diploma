import {ActionReducerMap} from '@ngrx/store';
import {AppState,} from './State';
import {UserReducers} from "./user/user.reducers";
import {FarmReducer} from "./farm/farm.reducer";
import {FieldsReducer} from "./field/fields.reducer";
import {CompanyReducer} from "./company/company.reducer";
import {CompanyUsersReducer} from "./company-user/company-users.reducer";
import {DeviceReducer} from "./device/device.reducer";
import {ThresholdPresetsReducer} from "./threshold-preset/threshold-preset.reducer";
import {DeviceDataReducer} from "./device-data/device-data.reducer";
import {UserCompanyUsersReducer} from "./user-company-user/user-company-user.reducer";
import {SubscriptionRequestsReducer} from "./subscription-request/subscription-request.reducer";
import {AppCompaniesReducer} from "./app-companies/app-companies.reducer";

export const appReducers: ActionReducerMap<AppState> = {
  devices: DeviceReducer,
  farms: FarmReducer,
  fields: FieldsReducer,
  thresholdPresets: ThresholdPresetsReducer,
  deviceData: DeviceDataReducer,

  companyUsers: CompanyUsersReducer,
  company: CompanyReducer,

  subscriptionRequests: SubscriptionRequestsReducer,
  appCompanies: AppCompaniesReducer,

  user: UserReducers,
  userCompanyUsers: UserCompanyUsersReducer,
};


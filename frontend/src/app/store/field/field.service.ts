import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {CompanyField} from "../../utilities/types/CompanyTypes";
import {CreateField, UpdateGeofence} from "../../utilities/types/MapTypes";

@Injectable({
  providedIn: 'root'
})
export class FieldService {

  constructor(private http: HttpClient) {
  }

  private controllerUrl = environment.apiBaseUrl + '/Field';

  public getFields(companyUserId: number): Observable<CompanyField[]> {
    return this.http.post<CompanyField[]>(this.controllerUrl + '/getFields', companyUserId);
  }

  public updateGeofence(request: UpdateGeofence): Observable<CompanyField> {
    return this.http.post<CompanyField>(this.controllerUrl + '/updateGeofence', request);
  }

  public createField(request: CreateField): Observable<CompanyField> {
    return this.http.post<CompanyField>(this.controllerUrl + '/createField', request);
  }

}

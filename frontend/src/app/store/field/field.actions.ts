import {createAction, props} from "@ngrx/store";
import {CompanyField} from "../../utilities/types/CompanyTypes";
import {CreateField, UpdateGeofence} from "../../utilities/types/MapTypes";

export const initLoadFields = createAction('[Fields] Init Load Fields', props<{ companyUserId: number }>());
export const initLoadFieldsSuccess = createAction('[[Fields] Init Load Fields Success', props<{ fields: CompanyField[] }>());
export const initLoadFieldsFailure = createAction('[Fields] Init Load Fields Failure', props<{ error: string | null }>());

export const updateGeofence = createAction('[Field] Update Field Geofence', props<{ geofence: UpdateGeofence }>());
export const updateGeofenceSuccess = createAction('[Field] Update Field Geofence Success', props<{ field: CompanyField }>());
export const updateGeofenceFailure = createAction('[Field] Update Field Geofence Failure', props<{ error: string | null }>());

export const createField = createAction('[Field] Create Field', props<{ field: CreateField }>())
export const createFieldSuccess = createAction('[Field] Create Field Success', props<{ field: CompanyField }>())
export const createFieldFailure = createAction('[Field] Create Field Failure', props<{ error: string }>())

export const fieldLogout = createAction('[Field] Logout');

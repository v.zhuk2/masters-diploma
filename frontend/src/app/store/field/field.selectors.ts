import {createFeatureSelector, createSelector} from '@ngrx/store';
import { FieldState} from '../State'

export const getFieldsState = createFeatureSelector<FieldState>('fields');

export const getFields = createSelector(getFieldsState, (state: FieldState) => state?.fields);

export const isLoadingFields = createSelector(getFieldsState, (state: FieldState) => state?.isLoading);

export const fieldsError = createSelector(getFieldsState, (state: FieldState) => state?.error);

export const getFieldById = (id: number) => createSelector(
  getFieldsState,
  (state: FieldState) => state?.fields?.find(x => x.id === id)
);

export const getFieldsByFarmId = (farmId: number) => createSelector(
  getFieldsState,
  (state: FieldState) => state?.fields?.filter(x => x.companyFarmId === farmId)
);

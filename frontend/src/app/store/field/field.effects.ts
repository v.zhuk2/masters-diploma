import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {
  createField, createFieldFailure, createFieldSuccess,
  initLoadFields,
  initLoadFieldsFailure,
  initLoadFieldsSuccess,
  updateGeofence, updateGeofenceFailure,
  updateGeofenceSuccess
} from "./field.actions";
import {FieldService} from "./field.service";

@Injectable()
export class FieldEffects {

  loadFields$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadFields),
      mergeMap(({companyUserId}) =>
        this.fieldService.getFields(companyUserId).pipe(
          map(fields =>
            initLoadFieldsSuccess({fields: fields})),
          catchError((error) =>
            of(initLoadFieldsFailure({error: error})))
        )
      )
    )
  );

  updateGeofence$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateGeofence),
      mergeMap((data) =>
        this.fieldService.updateGeofence(data.geofence).pipe(
          map(field => updateGeofenceSuccess({field: field})),
          catchError((error) =>
            of(updateGeofenceFailure({error: error})))
        )
      )
    )
  );

  createField$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createField),
      mergeMap((data) =>
        this.fieldService.createField(data.field).pipe(
          map(field => createFieldSuccess({field: field})),
          catchError((error) =>
            of(createFieldFailure({error: error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private fieldService: FieldService
  ) {}
}

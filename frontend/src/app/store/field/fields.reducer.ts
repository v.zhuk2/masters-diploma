import {createReducer, on} from '@ngrx/store';
import {FieldState} from '../State';
import {
  createField,
  createFieldFailure,
  createFieldSuccess,
  fieldLogout,
  initLoadFields,
  initLoadFieldsFailure,
  initLoadFieldsSuccess,
  updateGeofence,
  updateGeofenceFailure,
  updateGeofenceSuccess
} from "./field.actions";

export const InitialFieldState: FieldState = {
  fields: null,
  error: null,
  isLoading: true,
};

export const FieldsReducer = createReducer(
  InitialFieldState,
  on(initLoadFields, state => state),
  on(initLoadFieldsSuccess, (state, {fields}) => ({
    ...state, fields, error: null, isLoading: false
  })),
  on(initLoadFieldsFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(updateGeofence, state => state),
  on(updateGeofenceSuccess, (state, {field}) => {
    const updatedFields = state.fields.map(stateField => stateField.id === field.id ? {
      ...field,
      geofence: field.geofence
    } : stateField);
    return {
      ...state,
      fields: updatedFields,
      error: null,
      isLoading: false
    }
  }),
  on(updateGeofenceFailure, (state, {error}) => ({...state, error, isLoading: false})),
  on(createField, state => {
    return {...state, isLoading: true};
  }),
  on(createFieldSuccess, (state, {field}) => {
    const updatedFields = [...state.fields, field];
    return {
      ...state, fields: updatedFields, isLoading: false
    }
  }),
  on(createFieldFailure, (state, {error}) => {
    return {...state, error, isLoading: false};
  }),
  on(fieldLogout, state => {
    return InitialFieldState
  })
);




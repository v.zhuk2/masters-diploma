import {
  Company,
  CompanyDevice, CompanyDeviceData,
  CompanyFarm,
  CompanyFarmUser,
  CompanyField,
  CompanyThresholdPreset,
  CompanyUser, CompanyUserDeviceData, SubscriptionRequest,
  User
} from "../utilities/types/CompanyTypes";

export interface AppState {
  //for all company users
  devices: DeviceState;
  farms: FarmState;
  fields: FieldState;
  thresholdPresets: ThresholdPresetsState;
  deviceData: DeviceDataState;

  //for all company admins+
  companyUsers: CompanyUserState;

  //for company owner
  company: CompanyState;

  //for app admin
  appCompanies: AppCompanyState;
  subscriptionRequests: SubscriptionRequestState;

  //company user segregated
  user: UserState;
  userCompanyUsers: UserCompanyUserState;
}

export interface Loadable {
  error: null | string,
  isLoading: boolean,
}

export interface CompanyState extends Loadable {
  company: Company | null
}

export interface AppCompanyState extends Loadable {
  companies: Company[] | null
}

export interface SubscriptionRequestState extends Loadable {
  subscriptionRequests: SubscriptionRequest[] | null
}

export interface UserState extends Loadable {
  user: User | null,
}

export interface FarmState extends Loadable {
  farms: CompanyFarm[] | null,
}

export interface FieldState extends Loadable {
  fields: CompanyField[] | null,
}

export interface CompanyUserState extends Loadable {
  companyUsers: CompanyUser[] | null,
}

export interface UserCompanyUserState extends Loadable {
  companyUsers: CompanyUser[] | null,
  currentCompanyUser: CompanyUser | null
}

export interface DeviceState extends Loadable {
  devices: CompanyDevice[] | null,
}

export interface ThresholdPresetsState extends Loadable {
  thresholdPresets: CompanyThresholdPreset[] | null,
}

export interface DeviceDataState extends Loadable {
  deviceData: CompanyDeviceData[] | null
}


import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {CompanyDeviceData, LoadMoreDeviceData} from "../../utilities/types/CompanyTypes";
import {loadMoreDeviceData} from "./device-data.actions";

@Injectable({
  providedIn: 'root'
})
export class DeviceDataService {

  constructor(private http: HttpClient) {}

  private controllerUrl = environment.apiBaseUrl + '/DeviceData';

  public getDevicesData(companyUserId: number): Observable<CompanyDeviceData[]> {
    return this.http.post<CompanyDeviceData[]>(this.controllerUrl + '/getDevicesData', companyUserId);
  }

  public loadMoreDeviceData(device: LoadMoreDeviceData): Observable<CompanyDeviceData[]> {
    return this.http.post<CompanyDeviceData[]>(this.controllerUrl + '/loadMoreDeviceData', device);
  }
}

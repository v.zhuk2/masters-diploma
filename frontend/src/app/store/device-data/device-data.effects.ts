import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {
  initLoadDeviceData,
  initLoadDeviceDataFailure,
  initLoadDeviceDataSuccess,
  loadMoreDeviceData, loadMoreDeviceDataFailure, loadMoreDeviceDataSuccess
} from "./device-data.actions";
import {DeviceDataService} from "./device-data.service";

@Injectable()
export class DeviceDataEffects {

  loadDeviceData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadDeviceData),
      mergeMap(({companyUserId}) =>
        this.deviceDataService.getDevicesData(companyUserId).pipe(
          map(deviceData => initLoadDeviceDataSuccess({deviceData: deviceData})),
          catchError((error) => of(initLoadDeviceDataFailure({
            error: error
          })))
        )
      )
    )
  );

  loadMoreDeviceData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadMoreDeviceData),
      mergeMap(({device}) =>
        this.deviceDataService.loadMoreDeviceData(device).pipe(
          map(deviceData => loadMoreDeviceDataSuccess({deviceData: deviceData})),
          catchError((error) => of(loadMoreDeviceDataFailure({
            error: error
          })))
        )
      )
    )
  );

  constructor(private actions$: Actions,
              private deviceDataService: DeviceDataService) {
  }
}

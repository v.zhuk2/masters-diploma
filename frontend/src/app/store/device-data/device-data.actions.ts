import {createAction, props} from "@ngrx/store";
import {CompanyDeviceData, LoadMoreDeviceData} from "../../utilities/types/CompanyTypes";

export const initLoadDeviceData = createAction(
  '[DeviceData] Init Load DeviceData',
  props<{ companyUserId: number }>());
export const initLoadDeviceDataSuccess = createAction(
  '[DeviceData] Init Load DeviceData Success',
  props<{ deviceData: CompanyDeviceData[] }>());
export const initLoadDeviceDataFailure = createAction(
  '[DeviceData] Init Load DeviceData Failure',
  props<{ error: string | null }>());
export const loadMoreDeviceData = createAction(
  '[DeviceData] Load More DeviceData',
  props<{ device: LoadMoreDeviceData }>());
export const loadMoreDeviceDataSuccess = createAction(
  '[DeviceData] Load More DeviceData Success',
  props<{ deviceData: CompanyDeviceData[] }>());
export const loadMoreDeviceDataFailure = createAction(
  '[DeviceData] Load More DeviceData Failure',
  props<{ error: string | null }>());
export const deviceDataLogout = createAction('[DeviceData] Logout');

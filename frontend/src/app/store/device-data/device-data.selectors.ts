import { createFeatureSelector, createSelector } from '@ngrx/store';
import {DeviceDataState} from '../State'
export const getDeviceDataState = createFeatureSelector<DeviceDataState>('deviceData');

export const getDeviceData = createSelector(
  getDeviceDataState,
  (state: DeviceDataState) => state?.deviceData
);

export const isDeviceData = createSelector(
  getDeviceDataState,
  (state: DeviceDataState) => state?.isLoading
);

export const deviceDataError = createSelector(
  getDeviceDataState,
  (state: DeviceDataState) => state?.error
);

export const getDeviceDataByDeviceId = (deviceId: number) => createSelector(
  getDeviceDataState,
  (state: DeviceDataState) => state?.deviceData?.filter(x => x.deviceId === deviceId)
);


import {createReducer, on} from '@ngrx/store';
import {DeviceDataState} from '../State';
import {
  deviceDataLogout,
  initLoadDeviceData,
  initLoadDeviceDataFailure,
  initLoadDeviceDataSuccess, loadMoreDeviceData, loadMoreDeviceDataFailure, loadMoreDeviceDataSuccess
} from "./device-data.actions";

export const InitialDeviceDataState: DeviceDataState = {
  deviceData: null,
  error: null,
  isLoading: true,
};
//todo change load more to replace data instead of add
export const DeviceDataReducer = createReducer(
  InitialDeviceDataState,
  on(initLoadDeviceData, state => state),
  on(initLoadDeviceDataSuccess, (state, {deviceData}) => ({
    ...state, deviceData, error: null, isLoading: false
  })),
  on(initLoadDeviceDataFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(loadMoreDeviceData, state => state),
  on(loadMoreDeviceDataSuccess, (state, {deviceData}) => {
    const deviceId = deviceData[0]?.deviceId; // get deviceId from the first item in the new array
    let updatedDeviceData = state.deviceData;
    if (deviceId) {
      updatedDeviceData = updatedDeviceData.filter(data => data.deviceId !== deviceId); // remove old data of this device
      updatedDeviceData = [...updatedDeviceData, ...deviceData]; // add new data
    }
    return {...state, deviceData: updatedDeviceData, error: null, isLoading: false};
  }),
  on(loadMoreDeviceDataFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(deviceDataLogout, state => {
    return InitialDeviceDataState
  })
);

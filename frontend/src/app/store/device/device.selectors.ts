import { createFeatureSelector, createSelector } from '@ngrx/store';
import {DeviceState} from '../State'
export const getDevicesState = createFeatureSelector<DeviceState>('devices');

export const getDevices = createSelector(
  getDevicesState,
  (state: DeviceState) => state?.devices
);

export const isLoadingDevices = createSelector(
  getDevicesState,
  (state: DeviceState) => state?.isLoading
);

export const devicesError = createSelector(
  getDevicesState,
  (state: DeviceState) => state?.error
);

export const getDeviceById = (id: number) => createSelector(
  getDevicesState,
  (state: DeviceState) => state?.devices?.find(device => device.id === id)
);

export const getDeviceByFieldId = (fieldId: number) => createSelector(
  getDevicesState,
  (state: DeviceState) => state?.devices?.filter(device => device.fieldId === fieldId)
);


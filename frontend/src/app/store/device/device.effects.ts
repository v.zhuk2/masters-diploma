import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {
  assignDeviceToField,
  assignDeviceToFieldFailure,
  assignDeviceToFieldSuccess,
  createDevice,
  createDeviceFailure,
  createDeviceSuccess,
  initLoadDevices,
  initLoadDevicesFailure,
  initLoadDevicesSuccess
} from "./device.actions";
import {DeviceService} from "./device.service";
import {Store} from "@ngrx/store";

@Injectable()
export class DevicesEffects {

  loadDevices$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadDevices),
      mergeMap(({companyUserId}) =>
        this.deviceService.getDevices(companyUserId).pipe(
          map(devices => initLoadDevicesSuccess({devices: devices})),
          catchError((error) => of(initLoadDevicesFailure({
            error: error
          })))
        )
      )
    )
  );

  createDevice$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createDevice),
      mergeMap((data) =>
        this.deviceService.createDevice(data.device).pipe(
          map(device => createDeviceSuccess({device: device})),
          catchError((error) =>
            of(createDeviceFailure({error: error})))
        )
      )
    )
  );

  assignToField$ = createEffect(() =>
    this.actions$.pipe(
      ofType(assignDeviceToField),
      mergeMap((data) =>
        this.deviceService.assignToField(data.device).pipe(
          map(device => assignDeviceToFieldSuccess({device: device})),
          catchError((error) =>
            of(assignDeviceToFieldFailure({error: error})))
        )
      )
    )
  );

  constructor(private actions$: Actions,
              private deviceService: DeviceService) {
  }
}

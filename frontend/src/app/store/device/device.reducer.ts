import { createReducer, on } from '@ngrx/store';
import {DeviceState, } from '../State';
import {
  assignDeviceToField, assignDeviceToFieldFailure, assignDeviceToFieldSuccess,
  createDevice, createDeviceFailure, createDeviceSuccess,
  deviceLogout,
  initLoadDevices,
  initLoadDevicesFailure,
  initLoadDevicesSuccess
} from "./device.actions";
import {updateGeofence, updateGeofenceFailure, updateGeofenceSuccess} from "../field/field.actions";

export const InitialDeviceState: DeviceState = {
  devices: null,
  error: null,
  isLoading: true,
};

export const DeviceReducer = createReducer(
  InitialDeviceState,
  on(initLoadDevices, state => state),
  on(initLoadDevicesSuccess, (state, {devices, }) => ({
    ...state, devices, error: null, isLoading: false
  })),
  on(initLoadDevicesFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(createDevice, state => {
    return {...state, isLoading: true};
  }),
  on(createDeviceSuccess, (state, {device}) => {
    const updatedDevices = [...state.devices, device];
    return {
      ...state, devices: updatedDevices, isLoading: false
    }
  }),
  on(createDeviceFailure, (state, {error}) => {
    return {...state, error, isLoading: false};
  }),
  on(assignDeviceToField, state => state),
  on(assignDeviceToFieldSuccess, (state, {device}) => {
    const updatedDevices = state.devices.map(dev => dev.id === device.id ? device : dev);
    return {
      ...state,
      devices: updatedDevices,
      error: null,
      isLoading: false
    }
  }),
  on(assignDeviceToFieldFailure, (state, {error}) => ({...state, error, isLoading: false})),
  on(deviceLogout, state => {
    return InitialDeviceState
  })
);

import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {AssignToField, CompanyDevice, CreateDevice} from "../../utilities/types/CompanyTypes";

@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  constructor(private http: HttpClient) {}

  private controllerUrl = environment.apiBaseUrl + '/Device';

  public getDevices(companyUserId: number): Observable<CompanyDevice[]> {
    return this.http.post<CompanyDevice[]>(this.controllerUrl + '/getDevices', companyUserId);
  }

  public createDevice(device: CreateDevice): Observable<CompanyDevice> {
    return this.http.post<CompanyDevice>(this.controllerUrl + '/createDevice', device);
  }


  public assignToField(device: AssignToField): Observable<CompanyDevice> {
    return this.http.post<CompanyDevice>(this.controllerUrl + '/assignToField', device);
  }
}

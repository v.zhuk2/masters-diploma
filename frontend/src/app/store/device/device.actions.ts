import {createAction, props} from "@ngrx/store";
import {
  AssignToField,
  CompanyDevice, CompanyDeviceData,
  CompanyField,
  CreateDevice,
  LoadMoreDeviceData
} from "../../utilities/types/CompanyTypes";
import {UpdateGeofence} from "../../utilities/types/MapTypes";

export const initLoadDevices = createAction('[Devices] Init Load Devices',
  props<{companyUserId: number}>());
export const initLoadDevicesSuccess = createAction(
  '[Devices] Init Load Devices Success',
  props<{devices: CompanyDevice[] }>()
);
export const initLoadDevicesFailure = createAction(
  '[Devices] Init Load Devices Failure',
  props<{error: string | null}>()
);
export const deviceLogout = createAction('[Devices] Logout');

export const createDevice = createAction('[Devices] Create Device', props<{ device: CreateDevice }>())
export const createDeviceSuccess = createAction('[Devices] Create Device Success', props<{ device: CompanyDevice }>())
export const createDeviceFailure = createAction('[Devices] Create Device Failure', props<{ error: string }>())


export const assignDeviceToField = createAction('[Devices] Assign Device To Field', props<{ device: AssignToField }>());
export const assignDeviceToFieldSuccess = createAction('[Devices] Assign Device To Field Success', props<{ device: CompanyDevice }>());
export const assignDeviceToFieldFailure = createAction('[Devices] Assign Device To Field Failure', props<{ error: string | null }>());




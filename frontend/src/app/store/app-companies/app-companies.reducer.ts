import { createReducer, on } from '@ngrx/store';
import {AppCompanyState} from '../State';
import {
  appCompaniesLogout, createAppCompany, createAppCompanyFailure, createAppCompanySuccess,
  initLoadAppCompanies,
  initLoadAppCompaniesFailure,
  initLoadAppCompaniesSuccess
} from "./app-companies.actions";
export const InitialAppCompaniesState: AppCompanyState = {
  companies: null,
  error: null,
  isLoading: true,
};

export const AppCompaniesReducer = createReducer(
  InitialAppCompaniesState,
  on(initLoadAppCompanies, state => state),
  on(initLoadAppCompaniesSuccess, (state, {companies}) => ({
    ...state, companies, error: null, isLoading: false
  })),
  on(initLoadAppCompaniesFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(createAppCompany, state => {
    return {...state, isLoading: true};
  }),
  on(createAppCompanySuccess, (state, {company}) => {
    const updatedCompanies = [...state.companies, company];
    return {
      ...state, companies: updatedCompanies, isLoading: false
    }
  }),
  on(createAppCompanyFailure, (state, {error}) => {
    return {...state, error, isLoading: false};
  }),
  on(appCompaniesLogout, state => {
    return InitialAppCompaniesState
  })
);

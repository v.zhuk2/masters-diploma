import {createAction, props} from "@ngrx/store";
import {Company, CreateCompany} from "../../utilities/types/CompanyTypes";

export const initLoadAppCompanies = createAction('[AppCompanies] Init Load AppCompanies');
export const initLoadAppCompaniesSuccess = createAction(
  '[AppCompanies] Init Load AppCompanies Success',
  props<{ companies: Company[] }>()
);
export const initLoadAppCompaniesFailure = createAction(
  '[AppCompanies] Init Load AppCompanies Failure',
  props<{ error: string | null}>()
);
export const appCompaniesLogout = createAction('[AppCompanies] Logout');

export const createAppCompany = createAction('[AppCompanies] Create AppCompanies', props<{ company: CreateCompany }>())
export const createAppCompanySuccess = createAction('[AppCompanies] Create Farm AppCompanies', props<{ company: Company }>())
export const createAppCompanyFailure = createAction('[AppCompanies] Create Farm AppCompanies', props<{ error: string }>())

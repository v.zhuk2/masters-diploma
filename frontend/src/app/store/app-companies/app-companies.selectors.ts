import { createFeatureSelector, createSelector } from '@ngrx/store';
import {AppCompanyState} from '../State'
export const getAppCompaniesState = createFeatureSelector<AppCompanyState>('appCompanies');

export const getAppCompanies = createSelector(
  getAppCompaniesState,
  (state: AppCompanyState) => state?.companies
);

export const isLoadingAppCompanies = createSelector(
  getAppCompaniesState,
  (state: AppCompanyState) => state?.isLoading
);

export const appCompaniesError = createSelector(
  getAppCompaniesState,
  (state: AppCompanyState) => state?.error
);

export const getAppCompanyById = (id: number) => createSelector(
  getAppCompaniesState,
  (state: AppCompanyState) => state?.companies?.find(c => c.id === id)
);

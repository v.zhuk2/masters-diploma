import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {
  createAppCompany, createAppCompanyFailure, createAppCompanySuccess,
  initLoadAppCompanies,
  initLoadAppCompaniesFailure,
  initLoadAppCompaniesSuccess
} from "./app-companies.actions";
import {CompanyService} from "../company/company.service";
import {createFarm, createFarmFailure, createFarmSuccess} from "../farm/farm.actions";

@Injectable()
export class AppCompaniesEffects {

  loadAppCompanies$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadAppCompanies),
      mergeMap(() =>
        this.companyService.getCompanies().pipe(
          map(companies => initLoadAppCompaniesSuccess({companies: companies})),
          catchError((error) => of(initLoadAppCompaniesFailure({
            error: error
          })))
        )
      )
    )
  );

  createAppCompany$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createAppCompany),
      mergeMap((data) =>
        this.companyService.createCompany(data.company).pipe(
          map(company => createAppCompanySuccess({company: company})),
          catchError((error) =>
            of(createAppCompanyFailure({error: error})))
        )
      )
    )
  );

  constructor(private actions$: Actions,
              private companyService: CompanyService) {
  }
}

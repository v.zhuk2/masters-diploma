import {createFeatureSelector, createSelector} from '@ngrx/store';
import { FarmState} from '../State'

export const getFarmsState = createFeatureSelector<FarmState>('farms');

export const getFarms = createSelector(getFarmsState, (state: FarmState) => state?.farms);

export const isLoadingFarms = createSelector(getFarmsState, (state: FarmState) => state?.isLoading);

export const farmsError = createSelector(getFarmsState, (state: FarmState) => state?.error);

export const getFarmById = (id: number) => createSelector(
  getFarmsState,
  (state: FarmState) => state?.farms?.find(farm => farm.id === id)
);

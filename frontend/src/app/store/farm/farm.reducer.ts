import {createReducer, on} from '@ngrx/store';
import {FarmState} from '../State';
import {
  createFarm,
  createFarmFailure,
  createFarmSuccess,
  farmLogout,
  initLoadFarms,
  initLoadFarmsFailure,
  initLoadFarmsSuccess,
} from './farm.actions';

export const InitialFarmState: FarmState = {
  farms: null,
  error: null,
  isLoading: true,
};

export const FarmReducer = createReducer(
  InitialFarmState,
  on(initLoadFarms, state => state),
  on(initLoadFarmsSuccess, (state, {farms}) => ({
    ...state, farms, error: null, isLoading: false
  })),
  on(initLoadFarmsFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(createFarm, state => {
    return {...state, isLoading: true};
  }),
  on(createFarmSuccess, (state, {farm}) => {
    const updatedFarms = [...state.farms, farm];
    return {
      ...state, farms: updatedFarms, isLoading: false
    }
  }),
  on(createFarmFailure, (state, {error}) => {
    return {...state, error, isLoading: false};
  }),
  on(farmLogout, state => {
    return InitialFarmState
  })
);


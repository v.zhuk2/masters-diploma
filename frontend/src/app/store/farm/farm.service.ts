import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {CompanyFarm, CreateFarm,} from "../../utilities/types/CompanyTypes";

@Injectable({
  providedIn: 'root'
})
export class FarmService {

  constructor(private http: HttpClient) {
  }

  private controllerUrl = environment.apiBaseUrl + '/Farm';

  public createFarm(farm: CreateFarm): Observable<CompanyFarm> {
    return this.http.post<CompanyFarm>(this.controllerUrl + '/createFarm', farm);
  }

  public getFarms(companyUserId: number): Observable<CompanyFarm[]> {
    return this.http.post<CompanyFarm[]>(this.controllerUrl + '/getFarms', companyUserId);
  }
}

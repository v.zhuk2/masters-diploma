import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, from, of, switchMap} from "rxjs";
import {
  createFarm,
  createFarmFailure,
  createFarmSuccess,
  initLoadFarms,
  initLoadFarmsFailure,
  initLoadFarmsSuccess
} from "./farm.actions";
import {FarmService} from "./farm.service";
import {Action, Store} from "@ngrx/store";
import {getCurrentCompanyUser} from "../user-company-user/user-company-user.selectors";

@Injectable()
export class FarmEffects {

  loadFarms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadFarms),
      mergeMap(({companyUserId}) =>
        this.farmService.getFarms(companyUserId).pipe(
          map(farms =>
            initLoadFarmsSuccess({farms: farms})),
          catchError((error) =>
            of(initLoadFarmsFailure({error: error})))
        )
      )
    )
  );

  createFarm$ = createEffect(() =>
    this.actions$.pipe(
      ofType(createFarm),
      mergeMap((data) =>
        this.farmService.createFarm(data.farm).pipe(
          switchMap(farm =>
            this.store.select(getCurrentCompanyUser).pipe(
              switchMap(user => {
                const actions: Action[] = [
                  createFarmSuccess({farm}),
                ];
                return from(actions);
              })
            )
          ),
          catchError((error) => of(createFarmFailure({error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private farmService: FarmService,
    private readonly store: Store
  ) {
  }
}

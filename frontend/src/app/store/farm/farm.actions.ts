import {createAction, props} from "@ngrx/store";
import {CompanyFarm, CreateFarm} from "../../utilities/types/CompanyTypes";

export const initLoadFarms = createAction(
  '[Farms] Init Load Farms',
  props<{companyUserId: number}>());

export const initLoadFarmsSuccess = createAction(
  '[Farms] Init Load Farms Success',
  props<{ farms: CompanyFarm[] }>());

export const initLoadFarmsFailure = createAction(
  '[Farms] Init Load Farms Failure',
  props<{ error: string | null}>());
export const farmLogout = createAction('[Farms] Logout');

export const createFarm = createAction('[Farms] Create Farm', props<{ farm: CreateFarm }>())
export const createFarmSuccess = createAction('[Farms] Create Farm Success', props<{ farm: CompanyFarm }>())
export const createFarmFailure = createAction('[Farms] Create Farm Failure', props<{ error: string }>())

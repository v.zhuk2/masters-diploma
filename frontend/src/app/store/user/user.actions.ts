import {createAction, props} from '@ngrx/store';
import {User} from "../../utilities/types/CompanyTypes";


export const initLoadUser = createAction('[User] Init Load User');
export const initLoadUserSuccess = createAction(
  '[User] Init Load User Success',
  props<{ user: User }>()
);
export const initLoadUserFailure = createAction(
  '[User] Init Load User Failure',
  props<{ error: string | null }>()
);

export const userLogout = createAction('[User] Logout');

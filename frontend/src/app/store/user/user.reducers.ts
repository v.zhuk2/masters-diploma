import {createReducer, on} from '@ngrx/store';
import {initLoadUser, initLoadUserFailure, initLoadUserSuccess, userLogout} from './user.actions';
import {UserState} from "../State";

export const initialUserState: UserState = {
  user: null,
  error: null,
  isLoading: true
};

export const UserReducers = createReducer(
  initialUserState,
  on(initLoadUser, state => state),
  on(initLoadUserSuccess, (state, {user}) => ({...state, user, error: null, isLoading: false})),
  on(initLoadUserFailure, (state, {error}) => ({...state, error, isLoading: true})),
  on(userLogout, state => {
    return initialUserState
  })
);

import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {mergeMap} from 'rxjs/operators';
import {UserService} from "./user.service";
import {catchError, of} from "rxjs";
import {initLoadUser, initLoadUserFailure, initLoadUserSuccess, userLogout} from "./user.actions";
import {companyLogout} from "../company/company.actions";
import {companyUserLogout} from "../company-user/company-user.actions";
import {deviceLogout} from "../device/device.actions";
import {deviceDataLogout} from "../device-data/device-data.actions";
import {farmLogout} from "../farm/farm.actions";
import {fieldLogout} from "../field/field.actions";
import {thresholdPresetsLogout} from "../threshold-preset/threshold-preset.actions";
import {userCompanyUserLogout} from "../user-company-user/user-company-user.actions";
import {Action} from "@ngrx/store";
import {AppRoles} from "../../utilities/types/CompanyTypes";
import {initLoadAppCompanies} from "../app-companies/app-companies.actions";
import {initLoadSubscriptionRequests} from "../subscription-request/subscription-request.actions";

@Injectable()
export class UserEffects {

  loadUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadUser),
      mergeMap(() =>
        this.userService.getUser().pipe(
          mergeMap(user => {
            let actions: Action[] = [];
            if (user.appRole == AppRoles.AppAdmin) {
              actions.push(initLoadAppCompanies())
              actions.push(initLoadSubscriptionRequests())
            }
            actions.push(initLoadUserSuccess({user: user}))
            return actions;
          }),
          catchError((error) => of(initLoadUserFailure({
            error: error
          })))
        )
      )
    )
  );

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userLogout),
      mergeMap(() => {
        return of(
          companyLogout(),
          companyUserLogout(),
          deviceLogout(),
          deviceDataLogout(),
          farmLogout(),
          fieldLogout(),
          thresholdPresetsLogout(),
          userCompanyUserLogout(),
        );
      })
    )
  );

  constructor(private actions$: Actions,
              private userService: UserService) {
  }
}

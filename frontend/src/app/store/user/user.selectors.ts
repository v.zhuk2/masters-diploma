import { createFeatureSelector, createSelector } from '@ngrx/store';
import {UserState} from '../State'
export const getUserState = createFeatureSelector<UserState>('user');

export const getUser = createSelector(
  getUserState,
  (state: UserState) => state?.user
);

export const isLoadingUser = createSelector(
  getUserState,
  (state: UserState) => state?.isLoading
);

export const usersError = createSelector(
  getUserState,
  (state: UserState) => state?.error
);

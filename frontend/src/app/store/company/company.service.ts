import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {
  Company,
  CreateCompany,
  CreateRequestSubscription,
  SubscriptionRequest
} from "../../utilities/types/CompanyTypes";

@Injectable({
  providedIn: 'root',
})
export class CompanyService {

  constructor(private http: HttpClient) {}

  private controllerUrl = environment.apiBaseUrl + '/Company';

  public getCompany(companyUserId: number): Observable<Company> {
    return this.http.post<Company>(this.controllerUrl + '/getCompany', companyUserId);
  }

  public getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.controllerUrl + '/getCompanies');
  }

  public getSubRequests(): Observable<SubscriptionRequest[]> {
    return this.http.get<SubscriptionRequest[]>(this.controllerUrl + '/getSubRequests');
  }

  public createCompany(company: CreateCompany): Observable<Company> {
    return this.http.post<Company>(this.controllerUrl + '/createCompany', company);
  }

  public requestSubscription(request: CreateRequestSubscription): Observable<SubscriptionRequest> {
    return this.http.post<SubscriptionRequest>(this.controllerUrl + '/requestSubscription', request);
  }
}

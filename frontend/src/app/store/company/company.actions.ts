import {createAction, props} from "@ngrx/store";
import {Company} from "../../utilities/types/CompanyTypes";

export const initLoadCompany = createAction('[Company] Init Load Company', props<{companyUserId: number}>());
export const initLoadCompanySuccess = createAction(
  '[Company] Init Load Company Success',
  props<{ company: Company }>()
);
export const initLoadCompanyFailure = createAction(
  '[Company] Init Load Company Failure',
  props<{ error: string | null}>()
);
export const companyLogout = createAction('[Company] Logout');

import { createReducer, on } from '@ngrx/store';
import {CompanyState} from '../State';
import {companyLogout, initLoadCompany, initLoadCompanyFailure, initLoadCompanySuccess} from "./company.actions";

export const InitialCompanyState: CompanyState = {
  company: null,
  error: null,
  isLoading: true,
};

export const CompanyReducer = createReducer(
  InitialCompanyState,
  on(initLoadCompany, state => state),
  on(initLoadCompanySuccess, (state, {company}) => ({
    ...state, company, error: null, isLoading: false
  })),
  on(initLoadCompanyFailure, (state, {error}) => ({
    ...state, error, isLoading: true
  })),
  on(companyLogout, state => {
    return InitialCompanyState
  })
);

import { createFeatureSelector, createSelector } from '@ngrx/store';
import {CompanyState} from '../State'
export const getCompanyState = createFeatureSelector<CompanyState>('company');

export const getCompany = createSelector(
  getCompanyState,
  (state: CompanyState) => state?.company
);

export const isLoadingCompany = createSelector(
  getCompanyState,
  (state: CompanyState) => state?.isLoading
);

export const companyError = createSelector(
  getCompanyState,
  (state: CompanyState) => state?.error
);

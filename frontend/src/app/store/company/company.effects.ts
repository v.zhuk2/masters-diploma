import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap} from 'rxjs/operators';
import {catchError, of} from "rxjs";
import {initLoadCompany, initLoadCompanyFailure, initLoadCompanySuccess} from "./company.actions";
import {CompanyService} from "./company.service";

@Injectable()
export class CompanyEffects {

  loadCompany$ = createEffect(() =>
    this.actions$.pipe(
      ofType(initLoadCompany),
      mergeMap(({companyUserId}) =>
        this.companyService.getCompany(companyUserId).pipe(
          map(company => initLoadCompanySuccess({company: company})),
          catchError((error) => of(initLoadCompanyFailure({
            error: error
          })))
        )
      )
    )
  );

  constructor(private actions$: Actions,
              private companyService: CompanyService) {
  }
}

import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeRootComponent} from './home/home-root.component';
import {HomeRoutingModule} from './home-routing.module';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatSidenavModule} from "@angular/material/sidenav";
import {IoditMapComponent} from "../../components/iodit-map/iodit-map.component";
import {GoogleMapsModule} from "@angular/google-maps";
import {MatMenuModule} from "@angular/material/menu";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSelectModule} from "@angular/material/select";
import {ReactiveFormsModule} from "@angular/forms";
import {
  FinishPolygonCreationDialogComponent
} from "../../dialogs/finish-polyong-creation-dialog/finish-polygon-creation-dialog.component";
import {MatInputModule} from "@angular/material/input";
import {CompanyDevicesComponent} from "../../components/company-devices/company-devices.component";
import {
  CreateDeviceDialogComponent
} from "../../components/company-devices/create-device-dialog/create-device-dialog.component";
import {MatSliderModule} from "@angular/material/slider";
import {MatTableModule} from "@angular/material/table";
import {LandingPageComponent} from "../../components/landing-page/landing-page.component";
import {DeviceDetailsComponent} from "../../components/company-devices/device-details/device-details.component";
import {CompanyFieldsComponent} from "../../components/company-fields/company-fields.component";
import {AppCompaniesComponent} from "../../components/app-companies/app-companies.component";
import {
  AppCompanyDetailsComponent
} from "../../components/app-companies/app-company-details/app-company-details.component";
import {CompanyFarmsComponent} from "../../components/company-farms/company-farms.component";
import {CompanyThresholdsComponent} from "../../components/company-thresholds/company-thresholds.component";
import {FarmDetailsComponent} from "../../components/company-farms/farm-details/farm-details.component";
import {FieldDetailsComponent} from "../../components/company-fields/field-details/field-details.component";
import {
  ThresholdDetailsComponent
} from "../../components/company-thresholds/threshold-details/threshold-details.component";
import {UserCompanyComponent} from "../../components/user-company/user-company.component";
import {UserSettingsComponent} from "../../components/user-settings/user-settings.component";
import {CompanyUsersComponent} from "../../components/company-users/company-users.component";
import {
  CompanyUserDetailsComponent
} from "../../components/company-users/company-user-details/company-user-details.component";
import {
  CreateFarmDialogComponent
} from "../../components/company-farms/create-farm-dialog/create-farm-dialog.component";
import {
  CreateThresholdDialogComponent
} from "../../components/company-thresholds/create-threshold-dialog/create-threshold-dialog.component";
import {SubscriptionRequestComponent} from "../../components/subscription-request/subscription-request.component";
import {
  SubscriptionRequestDetailsComponent
} from "../../components/subscription-request/subscription-request-details/subscription-request-details.component";
import {
  CreateCompanyDialogComponent
} from "../../components/subscription-request/create-company-dialog/create-company-dialog.component";
import {RequestCompanyDialogComponent} from "../../dialogs/request-company-dialog/request-company-dialog.component";
import {
  InviteUserDialogComponent
} from "../../components/company-users/invite-user-dialog/invite-user-dialog.component";
import {
  DeviceAsignToFieldDialogComponent
} from "../../components/company-devices/device-details/device-asign-to-field-dialog/device-asign-to-field-dialog.component";
import {
  FieldDeviceAsignDialogComponent
} from "../../components/company-fields/field-details/field-device-asign-dialog/field-device-asign-dialog.component";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {NgxMatDatetimePickerModule, NgxMatNativeDateModule} from "@angular-material-components/datetime-picker";
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
import {MapFieldDetailsComponent} from "../../components/iodit-map/map-field-details/map-field-details.component";
import {MapFarmDetailsComponent} from "../../components/iodit-map/map-farm-details/map-farm-details.component";

@NgModule({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    HomeRootComponent,
    LandingPageComponent,
    IoditMapComponent,
    CompanyDevicesComponent,
    DeviceDetailsComponent,
    CompanyFarmsComponent,
    FarmDetailsComponent,
    CompanyFieldsComponent,
    FieldDetailsComponent,
    CompanyThresholdsComponent,
    ThresholdDetailsComponent,
    CompanyUsersComponent,
    CompanyUserDetailsComponent,
    AppCompaniesComponent,
    AppCompanyDetailsComponent,
    UserCompanyComponent,
    UserSettingsComponent,
    CreateDeviceDialogComponent,
    SubscriptionRequestComponent,
    SubscriptionRequestDetailsComponent,
    FinishPolygonCreationDialogComponent,
    CreateFarmDialogComponent,
    CreateThresholdDialogComponent,
    CreateCompanyDialogComponent,
    RequestCompanyDialogComponent,
    InviteUserDialogComponent,
    DeviceAsignToFieldDialogComponent,
    FieldDeviceAsignDialogComponent,
    MapFieldDetailsComponent,
    MapFarmDetailsComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatSidenavModule,
    GoogleMapsModule,
    MatMenuModule,
    MatDialogModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSliderModule,
    MatTableModule,
    NgxMatDatetimePickerModule,
    NgxMatNativeDateModule,
    MatDatepickerModule,
    MatCardModule,
    MatGridListModule
  ]
})
export class HomeModule {
}

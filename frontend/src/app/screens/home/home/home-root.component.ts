import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from "../../../services/api-services/auth-service";
import {Store} from "@ngrx/store";
import {Observable, Subscription, tap} from "rxjs";
import {
  AppRoles,
  CompanyRoles,
  CompanyUser,
  User
} from "../../../utilities/types/CompanyTypes";
import {FormControl} from "@angular/forms";
import {initLoadUser, userLogout} from "../../../store/user/user.actions";
import {MatDialog} from "@angular/material/dialog";
import {changeCurrentUser, initLoadUserCompanyUsers} from "../../../store/user-company-user/user-company-user.actions";
import {
  getCurrentCompanyUser,
  getUserCompanyUsers,
  isLoadingUserCompanyUsers
} from "../../../store/user-company-user/user-company-user.selectors";
import {getUser, isLoadingUser} from "../../../store/user/user.selectors";
import {CompanyService} from "../../../store/company/company.service";
import {RequestCompanyDialogComponent} from "../../../dialogs/request-company-dialog/request-company-dialog.component";
import {map} from "rxjs/operators";
import {SnackBarService} from "../../../services/snack-bar.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-home-root',
  templateUrl: './home-root.component.html',
  styleUrls: ['./home-root.component.scss'],
})
export class HomeRootComponent implements OnInit, OnDestroy, AfterViewInit {
  public $companyUsers: Observable<CompanyUser[]>;
  public $currentUser: Observable<CompanyUser>;
  public $user: Observable<User>
  companyUsersControl = new FormControl(null)
  subscriptions: Subscription[] = [];  // Create an array of subscriptions
  public companyRoles = CompanyRoles;
  public appRoles = AppRoles;
  public email: string = '';

  constructor(private readonly authService: AuthService,
              private readonly companyService: CompanyService,
              private readonly snackBarService: SnackBarService,
              private readonly router: Router,
              private readonly store: Store,
              private readonly dialog: MatDialog) {
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.store.dispatch(initLoadUserCompanyUsers());
    this.store.dispatch(initLoadUser());
    const sub1 = this.store.select(isLoadingUser).pipe().subscribe(x => {
      if (!x) {
        this.$user = this.store.select(getUser);
        this.$user.subscribe(x => {this.email = x.email})
      }
    })
    const sub2 = this.store.select(isLoadingUserCompanyUsers).pipe().subscribe(x => {
      if (!x) {
        this.$companyUsers = this.store.select(getUserCompanyUsers);
        this.$currentUser = this.store.select(getCurrentCompanyUser);
        const sub3 = this.$currentUser.subscribe(user => {
          this.companyUsersControl.setValue(user.id);
        })
        this.subscriptions.push(sub3);
      }
    })
    this.subscriptions.push(sub1);
    this.subscriptions.push(sub2);
  }

  currentUserChanged(newUserId: number, $event) {
    console.log($event);
    const sub2 = this.$companyUsers.pipe(
      tap(x => {
        this.store.dispatch(changeCurrentUser({currentUser: x.find(z => z.id == newUserId)}))
      })
    ).subscribe();
    this.subscriptions.push(sub2);  // Add subscription to the array
  }

  requestSubscription(): void {
    this.dialog.open(RequestCompanyDialogComponent, {
      width: '95%'
    }).afterClosed()
      .pipe(map(data => data as {name: string, maxDevices: number}))
      .subscribe({
        next: data => {
          if (data != null) {
              this.companyService.requestSubscription({name: data.name, email: this.email, maxDevices: data.maxDevices}).subscribe(x => {
                this.snackBarService.info("Subscription Request successful")
              }, error => this.snackBarService.error(error.message))
          }
        },
        error: x => {
          console.log(x);
        }
      });
  }

  async logout() {
    this.store.dispatch(userLogout());
    this.authService.logout();
    await this.router.navigateByUrl('/auth')//todo check if working
    window.location.reload();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());  // Unsubscribe all subscriptions
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeRootComponent } from './home/home-root.component';
import {AuthGuard} from "../../services/auth-guard";
import {IoditMapComponent} from "../../components/iodit-map/iodit-map.component";
import {CompanyFarmsComponent} from "../../components/company-farms/company-farms.component";
import {CompanyDevicesComponent} from "../../components/company-devices/company-devices.component";
import {LandingPageComponent} from "../../components/landing-page/landing-page.component";
import {DeviceDetailsComponent} from "../../components/company-devices/device-details/device-details.component";
import {CompanyFieldsComponent} from "../../components/company-fields/company-fields.component";
import {FieldDetailsComponent} from "../../components/company-fields/field-details/field-details.component";
import {FarmDetailsComponent} from "../../components/company-farms/farm-details/farm-details.component";
import {CompanyThresholdsComponent} from "../../components/company-thresholds/company-thresholds.component";
import {ThresholdDetailsComponent} from "../../components/company-thresholds/threshold-details/threshold-details.component";
import {AppCompaniesComponent} from "../../components/app-companies/app-companies.component";
import {AppCompanyDetailsComponent} from "../../components/app-companies/app-company-details/app-company-details.component";
import {UserCompanyComponent} from "../../components/user-company/user-company.component";
import {UserSettingsComponent} from "../../components/user-settings/user-settings.component";
import {CompanyUsersComponent} from "../../components/company-users/company-users.component";
import {CompanyUserDetailsComponent} from "../../components/company-users/company-user-details/company-user-details.component";
import {SubscriptionRequestComponent} from "../../components/subscription-request/subscription-request.component";
import {
  SubscriptionRequestDetailsComponent
} from "../../components/subscription-request/subscription-request-details/subscription-request-details.component";


const routes: Routes = [
  {
    path: '',
    component: HomeRootComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'landing', pathMatch: 'full'},
      { path: 'landing', component: LandingPageComponent},
      { path: 'map', component: IoditMapComponent },
      { path: 'devices', component: CompanyDevicesComponent},
      { path: 'device/:id', component: DeviceDetailsComponent},
      { path: 'farms', component: CompanyFarmsComponent },
      { path: 'farm/:id', component: FarmDetailsComponent },
      { path: 'fields', component: CompanyFieldsComponent },
      { path: 'field/:id', component: FieldDetailsComponent },
      { path: 'thresholds', component: CompanyThresholdsComponent },
      { path: 'threshold/:id', component: ThresholdDetailsComponent },
      { path: 'company-users', component: CompanyUsersComponent },
      { path: 'company-user/:id', component: CompanyUserDetailsComponent },
      { path: 'app-companies', component: AppCompaniesComponent },
      { path: 'app-company/:id', component: AppCompanyDetailsComponent },
      { path: 'subscriptions', component: SubscriptionRequestComponent },
      { path: 'subscription/:id', component: SubscriptionRequestDetailsComponent },
      { path: 'user-company', component: UserCompanyComponent },
      { path: 'user-settings', component: UserSettingsComponent },
      { path: '**', redirectTo: 'landing'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }

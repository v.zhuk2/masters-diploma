// auth/auth-routing.module.ts
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "../../components/login/login.component";
import {SignupPage} from "../../components/signup/signup.page";
import {RegSuccessfulComponent} from "../../components/signup/reg-successful/reg-successful.component";
import {AuthRootComponent} from "./auth-root/auth-root.component";

const routes: Routes = [
  {
    path: '',
    component: AuthRootComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'signup', component: SignupPage },
      { path: 'registration-successful', component: RegSuccessfulComponent },
      { path: '', redirectTo: 'login', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }

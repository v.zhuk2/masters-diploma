import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {AuthGuard} from "./services/auth-guard";


const routes: Routes = [
  { path: 'auth', loadChildren: () => import('./screens/auth/auth.module').then(m => m.AuthModule) },
  { path: 'home', loadChildren: () => import('./screens/home/home.module').then(m => m.HomeModule), canActivate: [AuthGuard] },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

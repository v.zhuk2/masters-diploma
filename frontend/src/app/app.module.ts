import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {JwtInterceptor} from "./services/jwt-interceptor";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatTabsModule} from "@angular/material/tabs";
import {MatInputModule} from "@angular/material/input";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatButtonModule} from "@angular/material/button";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {MatDialogModule} from "@angular/material/dialog";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {UserEffects} from "./store/user/user.effects";
import {FarmEffects} from "./store/farm/farm.effects";
import {appReducers} from "./store/root.reducers";
import {CompanyUserEffects} from "./store/company-user/company-user.effects";
import {FieldEffects} from "./store/field/field.effects";
import {DevicesEffects} from "./store/device/device.effects";
import {CompanyEffects} from "./store/company/company.effects";
import {DeviceDataEffects} from "./store/device-data/device-data.effects";
import {ThresholdPresetEffects} from "./store/threshold-preset/threshold-preset.effects";
import {MatSliderModule} from "@angular/material/slider";
import {UserCompanyUserEffects} from "./store/user-company-user/user-company-user.effects";
import {AppCompaniesEffects} from "./store/app-companies/app-companies.effects";
import {SubscriptionRequestEffects} from "./store/subscription-request/subscription-request.effects";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {GoogleMapsModule} from "@angular/google-maps";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatTabsModule,
    MatInputModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSliderModule,
    MatDatepickerModule,
    MatNativeDateModule,
    GoogleMapsModule,
    StoreModule.forRoot(appReducers),
    EffectsModule.forRoot([
      DevicesEffects,
      FarmEffects,
      FieldEffects,
      ThresholdPresetEffects,
      DeviceDataEffects,

      CompanyUserEffects,
      CompanyEffects,

      AppCompaniesEffects,
      SubscriptionRequestEffects,

      UserCompanyUserEffects,
      UserEffects,
    ]),
    StoreDevtoolsModule.instrument({maxAge: 25}),
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}],
  bootstrap: [AppComponent],
})
export class AppModule {
}

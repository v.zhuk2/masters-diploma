import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';
import {
  AuthResponse,
  UserLogin,
  UserRegistration,
  UserRegistrationResponse
} from "../../utilities/types/auth-types";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

export interface JWT {
  exp: number;
  iat: number;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(private http: HttpClient) {}

  private controllerUrl = environment.apiBaseUrl + '/Auth';

  login(credentials: UserLogin): Observable<AuthResponse> {
    return this.http.post<AuthResponse>(this.controllerUrl + '/login', credentials)
      .pipe(tap(res => {
        localStorage.setItem('access_token', res.token);
      }));
  }

  logout() {
    localStorage.removeItem('access_token');
  }

  public register(userData: UserRegistration): Observable<UserRegistrationResponse> {
    return this.http.post<UserRegistrationResponse>(this.controllerUrl + '/register', userData);
  }

  isAuthenticated(): boolean {
    const token = localStorage.getItem('access_token');
    if (!token) {
      return false;
    }

    try {
      let decoded = jwt_decode<JWT>(token);
      const expirationDate = decoded.exp * 1000;
      return Date.now() < expirationDate;
    } catch (err) {
      return false;
    }
  }

  getToken(): string {
    let token = localStorage.getItem('access_token');
    return token ? token : ''
  }

  getRefreshToken(): string {
    let token = localStorage.getItem('refresh_token');
    return token ? token : ''
  }
}

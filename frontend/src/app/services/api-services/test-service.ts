import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";

export interface JWT {
  exp: number;
  iat: number;
}

@Injectable({
  providedIn: 'root',
})
export class TestService {

  constructor(private http: HttpClient) {}

  private controllerUrl = environment.apiBaseUrl + '/Auth';

  public testAuth(): Observable<any> {
    return this.http.get<any>(this.controllerUrl + '/testauth');
  }

}

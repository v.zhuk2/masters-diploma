import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from "./api-services/auth-service";
import {catchError, Observable, throwError} from "rxjs";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Add authorization header with JWT token if available
    if (request.url.endsWith('/login')) {
      return next.handle(request);
    }

    const accessToken = this.authService.getToken();
    if (accessToken) {
      request = this.addAuthorizationHeader(request, accessToken);
    }

    return next.handle(request).pipe(
      catchError(error => {
        if (error.status === 401) {
          return this.handle401Error(request, next);
        } else {
          return throwError(error);
        }
      })
    );
  }

  private addAuthorizationHeader(request: HttpRequest<any>, accessToken: string): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        Authorization: `Bearer ${accessToken}`
      }
    });
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    //   let refreshToken = this.authService.getRefreshToken();
    //   if (refreshToken) {
    //     return this.authService.refreshAccessToken({refreshToken}).pipe(
    //       switchMap(() => {
    //         const accessToken = this.authService.getToken();
    //         request = this.addAuthorizationHeader(request, accessToken);
    //         return next.handle(request);
    //       }),
    //       catchError(error => {
    //         this.authService.logout();
    //         return throwError(error);
    //       })
    //     );
    //   }
    this.authService.logout();
    return throwError("No RefreshToken available");
  }
}

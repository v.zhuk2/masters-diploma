import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({ providedIn: 'root' })
export class SnackBarService {
  constructor(private snackBar: MatSnackBar) {
  }

  warn(message: string): void {
    let styles = ['pink-snackbar', 'pink-snackbar-button'];
    this.alert(message, styles);
  }

  success(message: string): void {
    let styles = ['green-snackbar', 'green-snackbar-button'];
    this.alert(message, styles);
  }

  error(message: string): void {
    let styles = ['red-snackbar', 'red-snackbar-button'];
    this.alert(message, styles);
  }

  info(message: string): void {
    let styles = ['blue-snackbar', 'blue-snackbar-button'];
    this.alert(message, styles);
  }

  alert(message: string, styles: string[]): void {
    this.snackBar.open(message, 'close', {
      verticalPosition: 'top',
      horizontalPosition: 'end',
      duration: 15000,
      panelClass: styles
    })
  }
}

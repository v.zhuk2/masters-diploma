import * as turf from '@turf/turf';

export interface Company {
  id: number,
  ownerId: number,
  companyName: string;
  ownerEmail: string;
  appId: string;
  appName: string;
  maxDevices: number;
}

export interface User {
  firstName: string,
  lastName: string,
  appRole: AppRoles,
  email: string,
  id: number
}

export interface CompanyFarm {
  id: number,
  name: string,
  companyId: number
}

export interface CompanyField {
  id: number,
  name: string,
  companyId: number,
  companyFarmId: number,
  geofence: turf.Polygon
}

export interface CompanyUser {
  id: number,
  userId: number,
  companyId: number,
  email: string
  companyName: string,
  companyRole: CompanyRoles,
  isDefault: boolean,
  firstName: string,
  lastName: string
}

export interface CompanyFarmUser {
  id: number,
  companyId: number,
  companyUserId: number,
  companyFarmId: number,
  companyFarmRole: CompanyFarmRoles,
}

export interface CompanyDevice {
  id: number,
  name: string,
  companyId: number,
  fieldId: number,
  farmId: number,
  devEUI: string,
  joinEUI: string,
  appKey: string,
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
  deviceData: CompanyDeviceData[]
}

export interface CompanyThresholdPreset {
  id: number,
  name: string;
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
  companyId: number;
}

export interface CompanyDeviceData {
  deviceId: number;
  sensor1: number;
  sensor2: number;
  batteryLevel: number;
  temperature: number;
  timeStamp: Date;
}

export interface CompanyUserDeviceData {
  id: number,
  deviceId: number;
  userId: number,
  humidity1Min: number;
  humidity1Max: number;
  humidity2Min: number;
  humidity2Max: number;
  batteryLevelMin: number;
  batteryLevelMax: number;
  temperatureMin: number;
  temperatureMax: number;
}

export interface SubscriptionRequest {
  id: number;
  companyName: string;
  email: string;
  userId: number;
  isFulfilled: boolean;
  maxDevices: number;
}

export enum AppRoles {
  AppUser = 0,
  AppAdmin = 1
}

export enum CompanyRoles {
  CompanyOwner = 0,
  CompanyAdmin = 1,
  CompanyUser = 2
}

export enum CompanyFarmRoles {
  FarmAdmin = 0,
  FarmUser = 1,
}

export interface CreateDevice {
  deviceEUI: string;
  deviceName: string;
  farmId: number;
  companyId: number;
  companyUserId: number;
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
}

export interface CreatePreset {
  name: string;
  companyId: number;
  companyUserId: number;
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
}

export interface CreateFarm {
  name: string;
  companyId: number;
  companyUserId: number;
}

export interface InviteUser {
  email: string;
  companyId: number;
  companyUserId: number;
  companyRole: CompanyRoles
}

export interface CreateCompany {
  name: string;
  email: string;
  maxDevices: number;
}

export interface CreateRequestSubscription {
  name: string;
  email: string;
  maxDevices: number;
}

export interface AssignToField {
  companyId: number,
  companyUserId: number,
  deviceEui: string,
  fieldId: number
}

export interface AssignUserToFarm {
  companyId: number,
  companyUserId: number,
  farmId: number,
  userId: number
  farmRole: CompanyFarmRoles
}

export interface LoadMoreDeviceData {
  start: Date,
  end: Date,
  devEUI: string,
  companyUserId: number
}

export interface DeviceDataSource {
  id: number,
  name: string,
  companyId: number,
  fieldId: number,
  fieldName: string,
  farmId: number,
  farmName: string,
}

export interface DeviceDetailedInfo {
  id: number,
  name: string,
  companyId: number,
  fieldId: number,
  fieldName: string,
  farmId: number,
  farmName: string,
  devEUI: string,
  joinEUI: string,
  appKey: string,
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
  deviceData: CompanyDeviceData[]
}

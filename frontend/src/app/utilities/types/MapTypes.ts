import {CompanyDevice, CompanyDeviceData, CompanyFarmRoles, CompanyRoles} from "./CompanyTypes";
import * as turf from "@turf/turf";

export interface MapCompanyUser {
  id: number,
  userId: number,
  companyId: number,
  companyRole: CompanyRoles,
  companyFarmUsers: MapCompanyFarmUser[];
}

export interface MapCompanyFarmUser {
  id: number,
  companyId: number,
  companyUserId: number,
  companyFarmId: number,
  companyFarmRole: CompanyFarmRoles,
  companyFarm: MapCompanyFarm
}

export interface MapCompanyFarm {
  id: number,
  name: string,
  companyId: number,
  fields: MapCompanyField[]
}

export interface MapCompanyField {
  id: number,
  name: string,
  companyId: number,
  companyFarmId: number,
  googlePolygon: google.maps.PolygonOptions,
  percentage: number,
  devices: CompanyDevice[]
}

export interface MapCompanyDevice {
  id: number,
  devEUI: string,
  joinEUI: string,
  appKey: string,
  name: string,
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
  companyId: number,
  fieldId: number,
  deviceData: CompanyDeviceData[]
}

export interface PolygonData {
  googlePolygon: google.maps.Polygon,
  percentage: number
}

export interface UpdateGeofence {
  geofence: turf.Polygon,
  companyUserId: number,
  companyFarmId: number,
  fieldId: number
}

export interface CreateField {
  name: string,
  companyUserId: number,
  companyFarmId: number,
  geofence: turf.Polygon
}

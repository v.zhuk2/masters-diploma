export interface UserRegistrationResponse {
  message: string;
}

export interface UserRegistration {
  email: string,
  password: string,
  firstName: string,
  lastName: string
}

export interface UserLogin {
  email: string,
  password: string
}

export interface AuthResponse {
  token: string
}

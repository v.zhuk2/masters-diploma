import * as turf from "@turf/turf";
import {CompanyFarmRoles, CompanyRoles} from "./CompanyTypes";

export interface StatefulCompanyUser {
  id: number,
  userId: number,
  companyId: number,
  companyRole: CompanyRoles,
  isDefault: boolean,
  companyFarmUsers: StatefulCompanyFarmUser[];
  companyUserDeviceData: StatefulCompanyDeviceData[]
}

export interface StatefulCompanyFarmUser {
  id: number,
  companyId: number,
  companyUserId: number,
  companyFarmId: number,
  companyFarmRole: CompanyFarmRoles,
  companyFarm: StatefulCompanyFarm
}

export interface StatefulCompanyFarm {
  id: number,
  name: string,
  companyId: number,
  fields: StatefulCompanyField[]
}

export interface StatefulCompanyField {
  id: number,
  name: string,
  companyId: number,
  companyFarmId: number,
  geofence:  turf.Polygon,
  devices: StatefulCompanyDevice[]
}

export interface StatefulCompanyDevice {
  id: number,
  devEUI: string,
  joinEUI: string,
  appKey: string,
  name: string,
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
  companyId: number,
  fieldId: number,
  deviceData: StatefulCompanyDeviceData[]
}

export interface StatefulCompanyDeviceData {
  deviceId: number;
  sensor1: number;
  sensor2: number;
  batteryLevel: number;
  temperature: number;
  timeStamp: Date;
}

export interface StatefulCompanyDeviceData {
  id: number,
  deviceId: number;
  userId: number;
  defaultHumidity1Min: number;
  defaultHumidity1Max: number;
  defaultHumidity2Min: number;
  defaultHumidity2Max: number;
  defaultBatteryLevelMin: number;
  defaultBatteryLevelMax: number;
  defaultTemperatureMin: number;
  defaultTemperatureMax: number;
}

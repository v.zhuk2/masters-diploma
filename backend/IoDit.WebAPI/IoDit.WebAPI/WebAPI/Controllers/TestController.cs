﻿using System.Security.Claims;
using IoDit.WebAPI.Persistence.Entities;
using IoDit.WebAPI.Persistence.Repositories;
using IoDit.WebAPI.WebAPI.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IoDit.WebAPI.WebAPI.Controllers;

[ApiController]
[Authorize]
[Route("[controller]")]
public class TestController : ControllerBase, IBaseController
{
    private readonly ITestService _testService;
    private readonly IIoDitRepository _repository;
    
    public TestController(
        ITestService testService,
        IIoDitRepository repository)
    {
        _testService = testService;
        _repository = repository;
    }
    
    [AllowAnonymous]//todo delete before pushing
    [HttpGet("populate")]
    public async Task<IActionResult> Populate()
    {
        return Ok(await _testService.ClearAllDataAsyncAndPopulate());
    }
    
    public async Task<User?> GetRequestDetails()
    {
        var claimsIdentity = User.Identity as ClaimsIdentity;
        var userIdClaim = claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier);
        var userId = userIdClaim?.Value;
        if (userId == null)
        {
            return null;
        }
        var user = await _repository.GetUserByEmail(userId);
        if (user != null)
        {
            return user;
        }

        return null;
    }
}
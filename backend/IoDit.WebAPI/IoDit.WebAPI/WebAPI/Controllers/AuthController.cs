﻿using System.Security.Claims;
using IoDit.WebAPI.Persistence.Entities;
using IoDit.WebAPI.Persistence.Repositories;
using IoDit.WebAPI.Utilities;
using IoDit.WebAPI.WebAPI.Models.Auth.Login;
using IoDit.WebAPI.WebAPI.Models.Auth.Register;
using IoDit.WebAPI.WebAPI.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace IoDit.WebAPI.WebAPI.Controllers;

[ApiController]
[Authorize]
[Route("[controller]")]
public class AuthController : ControllerBase, IBaseController
{
    private readonly ILogger<AuthController> _logger;
    private readonly IIoDitRepository _repository;
    private readonly IAuthService _authService;
    private readonly IJwtUtils _jwtUtils;

    public AuthController(
        ILogger<AuthController> logger,
        IIoDitRepository repository,
        IAuthService authService,
        IJwtUtils jwtUtils)
    {
        _logger = logger;
        _repository = repository;
        _authService = authService;
        _jwtUtils = jwtUtils;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public async Task<IActionResult> Login([FromBody] LoginRequestDto model)
    {
        var result = await _authService.Login(model);
        if (result == null)
        {
            return Unauthorized();
        }
        return Ok(result);
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public async Task<IActionResult> Register([FromBody] RegistrationRequestDto request)
    {
        var result = await _authService.Register(request);
        if (result.Message.IsNullOrEmpty())
        {
            return Conflict("Email Already in use");
        }

        return Ok(result.Message);
    }
    
    public async Task<User?> GetRequestDetails()
    {
        var claimsIdentity = User.Identity as ClaimsIdentity;
        var userIdClaim = claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier);
        var userId = userIdClaim?.Value;
        if (userId == null)
        {
            return null;
        }
        var user = await _repository.GetUserByEmail(userId);
        if (user != null)
        {
            return user;
        }

        return null;
    }
    
}
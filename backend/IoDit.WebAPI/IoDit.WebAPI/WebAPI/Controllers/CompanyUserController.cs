﻿using System.Security.Claims;
using IoDit.WebAPI.Persistence.Entities;
using IoDit.WebAPI.Persistence.Repositories;
using IoDit.WebAPI.WebAPI.Models.CompanyUser;
using IoDit.WebAPI.WebAPI.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IoDit.WebAPI.WebAPI.Controllers;

[ApiController]
[Authorize]
[Route("[controller]")]
public class CompanyUserController : ControllerBase, IBaseController
{
    private readonly ILogger<CompanyUserController> _logger;
    private readonly IConfiguration _configuration;
    private readonly ICompanyUserService _companyUserService;
    private readonly IIoDitRepository _repository;

    public CompanyUserController(
        ILogger<CompanyUserController> logger,
        IConfiguration configuration, ICompanyUserService companyUserService, IIoDitRepository repository)
    {
        _logger = logger;
        _configuration = configuration;
        _companyUserService = companyUserService;
        _repository = repository;
    }

    [HttpGet("getUserCompanyUsers")]
    public async Task<IActionResult> GetUserCompanyUsers()
    {
        var user = await GetRequestDetails();
        if (user == null)
        {
            return BadRequest("Cannot find user identity");
        }

        var result = await _companyUserService.GetUserCompanyUsers(user.Email);
        if (result == null)
        {
            return NoContent();
        }

        return Ok(result);
    }

    [HttpPost("getCompanyUsers")]
    public async Task<IActionResult> GetCompanyUsers([FromBody] long companyUserId)
    {
        var user = await GetRequestDetails();
        if (user == null)
        {
            return BadRequest("Cannot find user identity");
        }

        var companyUser = await _repository.GetCompanyUserForUserSecure(user.Email, companyUserId);
        if (companyUser == null)
        {
            return BadRequest("Cannot access this feature, please contact your company admin");
        }

        return Ok(await _companyUserService.GetCompanyUsers(companyUser.CompanyId));
    }

    [HttpPost("inviteUser")]
    public async Task<IActionResult> InviteUser([FromBody] InviteUserRequestDto request)
    {
        var user = await GetRequestDetails();
        if (user == null)
        {
            return BadRequest("Cannot find user identity");
        }

        var companyUser = await _repository.GetCompanyUserForUserSecure(user.Email, request.CompanyUserId);
        if (companyUser == null)
        {
            return BadRequest("Cannot access this feature, please contact your company owner");
        }
        
        var invitedUser = await _repository.GetUserByEmail(request.Email);
        if (invitedUser == null)
        {
            return NotFound("Cannot find user with this email on app");
        }

        var invitedCompanyUser = await _repository.GetCompanyUserForUserByCompanyId(request.Email, request.CompanyId);
        if (invitedCompanyUser != null)
        {
            return Conflict("User already part of this company");
        }

        return Ok(await _companyUserService.InviteUser(request));
    }

    public async Task<User?> GetRequestDetails()
    {
        var claimsIdentity = User.Identity as ClaimsIdentity;
        var userIdClaim = claimsIdentity?.FindFirst(ClaimTypes.NameIdentifier);
        var userId = userIdClaim?.Value;
        if (userId == null)
        {
            return null;
        }

        var user = await _repository.GetUserByEmail(userId);
        if (user != null)
        {
            return user;
        }

        return null;
    }
}
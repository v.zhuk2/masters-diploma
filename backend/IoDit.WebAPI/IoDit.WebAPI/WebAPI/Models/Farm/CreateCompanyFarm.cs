﻿namespace IoDit.WebAPI.WebAPI.Models.Farm;

public class CreateCompanyFarm
{
    public string Name { get; set; }
    public string Email { get; set; }
    public long CompanyId { get; set; }
}
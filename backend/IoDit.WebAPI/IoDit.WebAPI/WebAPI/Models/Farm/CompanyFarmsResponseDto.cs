﻿namespace IoDit.WebAPI.WebAPI.Models.Farm;

public class CompanyFarmsResponseDto
{
    public long Id { get; set; }
    public string Name { get; set; }
    public long CompanyId { get; set; }
}
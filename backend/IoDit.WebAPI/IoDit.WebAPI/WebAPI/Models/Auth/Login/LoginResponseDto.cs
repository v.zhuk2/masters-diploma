﻿namespace IoDit.WebAPI.WebAPI.Models.Auth.Login;

public class LoginResponseDto
{
    public string Token { get; set; }
    public string RefreshToken { get; set; }
}
﻿namespace IoDit.WebAPI.WebAPI.Models.Auth;

public class RefreshTokenRequestDto
{
    public string RefreshToken { get; set; }
}
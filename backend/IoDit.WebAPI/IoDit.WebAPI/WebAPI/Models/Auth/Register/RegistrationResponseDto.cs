﻿namespace IoDit.WebAPI.WebAPI.Models.Auth.Register;

public class RegistrationResponseDto
{
    public string Message { get; set; }
}
﻿namespace IoDit.WebAPI.WebAPI.Models.CompanyUser;

public class GetCompanyUsersResponseDto
{
    public long Id { get; set; }
    public long UserId { get; set; }
    public long CompanyId { get; set; }
    public string CompanyName { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public bool IsDefault { get; set; }
}
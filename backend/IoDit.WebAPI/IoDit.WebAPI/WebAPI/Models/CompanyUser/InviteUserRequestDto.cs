﻿namespace IoDit.WebAPI.WebAPI.Models.CompanyUser;

public class InviteUserRequestDto
{
    public string Email { get; set; }
    public long CompanyUserId { get; set; }
    public long CompanyId { get; set; }
}
﻿using IoDit.WebAPI.Persistence.Entities;
using IoDit.WebAPI.Persistence.Entities.Company;

namespace IoDit.WebAPI.WebAPI.Services.Interfaces;

public interface ITestService
{
    Task<User?> ClearAllDataAsyncAndPopulate();
    Task<string> ClearAllDataAsync();
}
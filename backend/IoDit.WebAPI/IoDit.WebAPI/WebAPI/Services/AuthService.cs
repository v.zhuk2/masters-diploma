﻿using IoDit.WebAPI.Persistence.Entities;
using IoDit.WebAPI.Persistence.Repositories;
using IoDit.WebAPI.Utilities;
using IoDit.WebAPI.Utilities.Services;
using IoDit.WebAPI.WebAPI.Models.Auth.Login;
using IoDit.WebAPI.WebAPI.Models.Auth.Register;
using IoDit.WebAPI.WebAPI.Services.Interfaces;

namespace IoDit.WebAPI.WebAPI.Services;

public class AuthService : IAuthService
{
    private readonly IIoDitRepository _repository;
    private readonly IJwtUtils _jwtUtils;

    public AuthService(IIoDitRepository repository, IJwtUtils jwtUtils)
    {
        _repository = repository;
        _jwtUtils = jwtUtils;
    }
    
    public async Task<LoginResponseDto?> Login(LoginRequestDto request)
    {
        var user = await _repository.GetUserByEmail(request.Email);
        if (user == null)
        {
            return null;
        }

        var pass = PasswordEncoder.HashPassword(request.Password);

        if (PasswordEncoder.CheckIfSame(pass, user.Password))
        {
            return null;
        }

        return new LoginResponseDto()
        {
            Token = _jwtUtils.GenerateJwtToken(request.Email)
        };
    }

    public async Task<RegistrationResponseDto> Register(RegistrationRequestDto request)
    {
        var existedUser = await _repository.GetUserByEmail(request.Email);
        if (existedUser != null)
            return new RegistrationResponseDto
            {
                Message = "",
            };

        var user = new User
        {
            Email = request.Email,
            // Make sure to hash the password before storing it
            Password = PasswordEncoder.HashPassword(request.Password), 
            FirstName = request.FirstName,
            LastName = request.LastName,
        };
       var createdUser = await _repository.CreateAsync(user);
        return new RegistrationResponseDto()
        {
            Message = $"User successfully Registered for {createdUser.Email}",
        };
    }
}
﻿using IoDit.WebAPI.Persistence.Entities;
using IoDit.WebAPI.Persistence.Entities.Company;
using IoDit.WebAPI.Persistence.Repositories;
using IoDit.WebAPI.Utilities.Services;
using IoDit.WebAPI.Utilities.Types;
using IoDit.WebAPI.WebAPI.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite.Geometries;

namespace IoDit.WebAPI.WebAPI.Services;

public class TestService : ITestService
{
    private readonly IIoDitRepository _repository;

    public TestService(IIoDitRepository repository)
    {
        _repository = repository;
    }

    public async Task<string> ClearAllDataAsync()
    {
        var dbContext = _repository.DbContext;

        // Remove data from all tables
        dbContext.CompanyDeviceData.RemoveRange(dbContext.CompanyDeviceData);
        dbContext.CompanyFields.RemoveRange(dbContext.CompanyFields);
        dbContext.CompanyFarms.RemoveRange(dbContext.CompanyFarms);
        dbContext.CompanyDevices.RemoveRange(dbContext.CompanyDevices);
        dbContext.CompanyUsers.RemoveRange(dbContext.CompanyUsers);
        dbContext.Companies.RemoveRange(dbContext.Companies);
        dbContext.Users.RemoveRange(dbContext.Users);
        await _repository.SaveChangesAsync();
        return "success";
    }


    public async Task<User?> ClearAllDataAsyncAndPopulate()
    {
        await ClearAllDataAsync();
        var createdUser = await AddUser();
        // var createdCompany = await AddCompany(createdUser);
        // var createdCompanyUser = await AddCompanyUser(createdUser, createdCompany);
        // var createdCompanyFarm = await AddFarm(createdCompany);
        // var createdCompanyField = await AddField(createdCompany, createdCompanyFarm);
        // var createdDevice = await AddDevice(createdCompany);
        // var createdThresholdPreset = await AddThreshold(createdCompany);
        
        var res = await _repository.DbContext.Users
            .Include(x => x.Companies)
            //.ThenInclude(x => x.CompanyFarms)
            //.ThenInclude(x => x.Fields)
            //.ThenInclude(x => x.Devices)
            //.ThenInclude(x => x.DeviceData)
            .FirstOrDefaultAsync(x => x.Id == 1);
        return res;
    }
    
        private async Task<User> AddUser()
    {
        var user = new User()
        {
            Email = "viktor@mail.com",
            Password = PasswordEncoder.HashPassword("test"),
            FirstName = "Viktor",
            AppRole = AppRoles.AppAdmin,
            LastName = "Zhuk",
        };
        var createdUser = await _repository.CreateAsync(user);
        return createdUser;
    }

    private async Task<Company> AddCompany(User user)
    {
        var company = new Company()
        {
            Owner = user,
            OwnerId = user.Id,
            MaxDevices = 10,
            CompanyName = "ViktorTestCompany",
        };
        var createdCompany = await _repository.CreateAsync(company);
        return createdCompany;
    }
    
    private async Task<CompanyThresholdPreset> AddThreshold(Company company)
    {
        var thresholdPreset = new CompanyThresholdPreset()
        {
            Company = company,
            CompanyId = company.Id,
            Name = "Carrot sensor preset",
            DefaultHumidity1Max = 100,
            DefaultHumidity1Min = 10,
            DefaultHumidity2Max = 90,
            DefaultHumidity2Min = 20,
            DefaultTemperatureMax = 50,
            DefaultTemperatureMin = -50,
            DefaultBatteryLevelMax = 100,
            DefaultBatteryLevelMin = 15
        };
        var createdThresholdPreset = await _repository.CreateAsync(thresholdPreset);
        return createdThresholdPreset;
    }
    
    private async Task<CompanyUser> AddCompanyUser(User user, Company company)
    {
        var companyUser = new CompanyUser()
        {
            Company = company,
            User = user,
            CompanyId = company.Id,
            UserId = user.Id,
            IsDefault = true
        };
        var createdCompanyUser = await _repository.CreateAsync(companyUser);
        return createdCompanyUser;
    }
    
    private async Task<CompanyFarm> AddFarm(Company company)
    {
        var companyFarm = new CompanyFarm()
        {
            Company = company,
            Name = "Viktor Test Farm",
            CompanyId = company.Id,
        };
        var createdCompanyFarm = await _repository.CreateAsync(companyFarm);
        return createdCompanyFarm;
    }
    
    private async Task<CompanyField> AddField(Company company, CompanyFarm companyFarm)
    {
        var coordinates = new[]
        {
            new Coordinate(49.24462288608182, 28.431549956314093),
            new Coordinate(49.24303993069276, 28.435755660049445),
            new Coordinate(49.24528827253497, 28.43734889220429),
            new Coordinate(49.24571551595151,  28.43658178042603),
            new Coordinate(49.24900026398981,  28.43560545634461),
            new Coordinate(49.24462288608182, 28.431549956314093) // Must close the polygon by repeating the first point
        };
        var linearRing = new LinearRing(coordinates);
        var polygon = new Polygon(linearRing);
        var field = new CompanyField()
        {
            Geofence = polygon,
            Name = "test field 1",
            CompanyId = company.Id,
            CompanyFarm = companyFarm,
            CompanyFarmId = companyFarm.Id,
            Company = company
        };
        var createdField = await _repository.CreateAsync(field);
        return createdField;
    }
    
    private async Task<CompanyDevice> AddDevice(Company company)
    {
        var device = new CompanyDevice()
        {
            Company = company,
            CompanyId = company.Id,
            Name = "9F8E7D6C5B4A3B2C",
            DevEUI = "9F8E7D6C5B4A3B2C",
            DefaultHumidity1Max = 100,
            DefaultHumidity1Min = 10,
            DefaultHumidity2Max = 90,
            DefaultHumidity2Min = 20,
            DefaultTemperatureMax = 50,
            DefaultTemperatureMin = -50,
            DefaultBatteryLevelMax = 100,
            DefaultBatteryLevelMin = 15,
        };
        var createdDevice = await _repository.CreateAsync(device);
        return createdDevice;
    }
}
﻿using IoDit.WebAPI.Persistence.Entities.Company;
using IoDit.WebAPI.Persistence.Repositories;
using IoDit.WebAPI.WebAPI.Models.Farm;
using IoDit.WebAPI.WebAPI.Services.Interfaces;

namespace IoDit.WebAPI.WebAPI.Services;

public class FarmService : IFarmService
{
    private readonly IIoDitRepository _repository;

    public FarmService(IIoDitRepository repository)
    {
        _repository = repository;
    }

    public async Task<CompanyFarmsResponseDto> CreateCompanyFarm(CreateCompanyFarm request)
    {
        var company = await _repository.GetCompanyById(request.CompanyId);
        if (company == null)
        {
            return null;
        }
        var companyFarm = new CompanyFarm()
        {
            Company = company,
            Name = request.Name,
            CompanyId = company.Id,
        };
        var createdCompanyFarm = await _repository.CreateAsync(companyFarm);

        return new CompanyFarmsResponseDto()
        {
            Id = createdCompanyFarm.Id,
            Name = createdCompanyFarm.Name,
            CompanyId = createdCompanyFarm.CompanyId
        };
    }

    public async Task<List<CompanyFarmsResponseDto>?> GetCompanyFarms(long companyId)
    {
        var farms = await _repository.GetCompanyFarms(companyId);
        if (!farms.Any())
        {
            return new List<CompanyFarmsResponseDto>();
        }

        return farms.Select(x => new CompanyFarmsResponseDto()
        {
            Id = x.Id,
            Name = x.Name,
            CompanyId = x.CompanyId
        }).ToList();
    }
}
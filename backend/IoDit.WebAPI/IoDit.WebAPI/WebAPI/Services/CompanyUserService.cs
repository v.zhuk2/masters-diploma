﻿using IoDit.WebAPI.Persistence.Entities.Company;
using IoDit.WebAPI.Persistence.Repositories;
using IoDit.WebAPI.WebAPI.Models.CompanyUser;
using IoDit.WebAPI.WebAPI.Services.Interfaces;

namespace IoDit.WebAPI.WebAPI.Services;

public class CompanyUserService: ICompanyUserService
{
    private readonly IIoDitRepository _repository;

    public CompanyUserService(IIoDitRepository repository)
    {
        _repository = repository;
    }

    public async Task<List<GetCompanyUsersResponseDto>?> GetUserCompanyUsers(string email)
    {
        var companyUsers = await _repository.GetUserCompanyUsers(email);
        if (!companyUsers.Any())
        {
            return null;
        }
        return companyUsers.Select(x => new GetCompanyUsersResponseDto()
        {
                Id = x.Id,
                CompanyId = x.CompanyId,
                CompanyName = x.Company.CompanyName,
                IsDefault = x.IsDefault,
                UserId = x.UserId,
                FirstName = x.User.FirstName,
                LastName = x.User.LastName,
                Email = x.User.Email
        }).ToList();
    }
    
    public async Task<List<GetCompanyUsersResponseDto>?> GetCompanyUsers(long companyId)
    {
        var companyUsers = await _repository.GetCompanyUsers(companyId);
        if (!companyUsers.Any())
        {
            return null;
        }
        return companyUsers.Select(x => new GetCompanyUsersResponseDto()
        {
            Id = x.Id,
            CompanyId = x.CompanyId,
            CompanyName = x.Company.CompanyName,
            IsDefault = x.IsDefault,
            UserId = x.UserId,
            FirstName = x.User.FirstName,
            LastName = x.User.LastName,
            Email = x.User.Email
        }).ToList();
    }

    public async Task<GetCompanyUsersResponseDto?> InviteUser(InviteUserRequestDto request)
    {
        //todo send email?
        
        var invitedUserCompanyUsers = await _repository.GetUserCompanyUsers(request.Email);
        bool isDefault = invitedUserCompanyUsers.FirstOrDefault(x => x.IsDefault == true) == null;

        var company = await _repository.GetCompanyById(request.CompanyId);
        if (company == null)
        {
            return null;
        }
        var invitedUser = await _repository.GetUserByEmail(request.Email);
        
        var companyUser = new CompanyUser()
        {
            Company = company,
            User = invitedUser,
            CompanyId = company.Id,
            UserId = invitedUser.Id,
            IsDefault = isDefault,
        };
        var createdCompanyUser = await _repository.CreateAsync(companyUser);
        
        return new GetCompanyUsersResponseDto()
        {
            Id = createdCompanyUser.Id,
            CompanyId = createdCompanyUser.CompanyId,
            CompanyName = company.CompanyName,
            FirstName = invitedUser.FirstName,
            IsDefault = createdCompanyUser.IsDefault,
            LastName = invitedUser.LastName,
            UserId = invitedUser.Id,
            Email = invitedUser.Email
        };
    }
}
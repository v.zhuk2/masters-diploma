﻿namespace IoDit.WebAPI.Utilities;

public interface IJwtUtils
{
    public string GenerateJwtToken(string email);
}
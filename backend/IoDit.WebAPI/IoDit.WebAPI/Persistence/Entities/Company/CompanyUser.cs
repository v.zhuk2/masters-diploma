﻿using IoDit.WebAPI.Persistence.Entities.Base;

namespace IoDit.WebAPI.Persistence.Entities.Company;

public class CompanyUser : EntityBase, IEntity
{
    public long UserId { get; set; }
    public User User { get; set; }
    public long CompanyId { get; set; }
    public Company Company { get; set; }
    public bool IsDefault { get; set; }
    public ICollection<CompanyFarm> CompanyFarms { get; set; } = new List<CompanyFarm>();

}